<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('product', ['LFM-100', 'ME-Y30K', 'HS-2300']);
            $table->enum('plan', ['10G', '20G']);
            $table->char('contract', 3);
            $table->enum('customer_type', ['personal', 'business']);
            $table->string('name', 50);
            $table->string('telephone', 20)->nullable();
            $table->string('mobile', 20);
            $table->char('postcode', 5);
            $table->string('address');
            $table->string('address_detail');
            $table->string('recommander', 50)->nullable();
            $table->enum('payment_type', ['card', 'bank'])->nullable();
            $table->string('card_type', 20)->nullable();
            $table->string('card_number', 20)->nullable();
            $table->date('expire_date')->nullable();
            $table->string('bank_name', 20)->nullable();
            $table->string('bank_account_number', 100)->nullable();
            $table->text('memo')->nullable();
            $table->enum('status', ['waiting', 'accepted', 'ready_for_ship', 'sent'])->default('waiting');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
