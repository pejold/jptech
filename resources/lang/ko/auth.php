<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => '아이디 또는 비밀번호가 올바르지 않습니다.',
    'throttle' => '너무 많은 로그인 실패를 하셨습니다. :seconds 초 후에 다시 시도하세요.',

];
