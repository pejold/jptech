@extends('layout')

@section('title', ":: 캐러셀 이미지 수정")

@section('style')
<style type="text/css">
    .thumbnail {
        width: 100%;
    }
</style>
@endsection

@section('content')
    <div class="container standalone">
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/carousel">캐러셀 이미지</a></li>
            <li class="active">{{ $image->id }}번 이미지 수정</li>
        </ol>

        <form action="/carousel/{{ $image->id }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <div class="row">
                <div class="col-sm-6 col-md-8">
                    <div class="form-group {{ ($errors->has('image')) ? 'has-error' : '' }}">
                        <label for="image_field" class="control-label">변경할 이미지<small></small></label>
                        <input id="image_field" name="image" type="file" class="form-control" value="{{ old('image') }}">
                        <span class="help-block"><small>{{$errors->first('image')}}</small></span>
                    </div>
                    <button type="submit" class="btn btn-primary pull-right">이미지 수정</button>
                </div>
                <div class="col-sm-6 col-md-4">
                    <img src="{{ $image->image_url }}" alt="이미지 미리보기" id="image_preview" class="thumbnail narrow">
                </div>
            </div>
        </form>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(function () {
            $('input[type=file]').change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    var prev = $('#'+this.name+'_preview');
                    reader.onload = function (e) {
                        prev.attr('src', e.target.result);
                    }
                    reader.readAsDataURL(this.files[0]);
                }
            });
        });
    </script>
@endsection