@extends('layout')

@section('title', ":: 캐러셀 이미지")

@section('style')
<style type="text/css">
    .thumbnail {
        max-width: 100%;
        max-height: 360px;
        margin: 0 auto;
    }

    td form {
        margin-top: 5px;
    }
</style>
@endsection

@section('content')
    <div class="container standalone">
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li class="active">캐러셀 이미지</li>
        </ol>

        <table class="table table-bordered table-condensed">
            <thead>
                <tr>
                    <th width="60" class="text-center">순번</th>
                    <th class="text-center">이미지</th>
                    <th width="100" class="text-center">관리</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($images as $image)
                <tr>
                    <td class="text-center">{{ $image->id }}</td>
                    <td class="text-center"><img src="{{ $image->image_url }}" class="thumbnail"></td>
                    <td>
                        <div><a href="/carousel/{{ $image->id }}/edit" class="btn btn-sm btn-block btn-default">수정</a></div>
                        <div>
                            <form action="/carousel/{{ $image->id }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn btn-sm btn-block btn-danger">삭제</button>
                            </form>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        <div class="text-right">
            <a href="/carousel/create" class="btn btn-default">이미지 추가</a>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(function () {
            $('input[type=file]').change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    var prev = $('#'+this.name+'_preview');
                    reader.onload = function (e) {
                        prev.attr('src', e.target.result);
                    }
                    reader.readAsDataURL(this.files[0]);
                }
            });
        });
    </script>
@endsection