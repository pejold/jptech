@extends('layout')

@section('title', ":: 캐러셀 이미지 추가")

@section('style')
<style type="text/css">
    .thumbnail {
        width: 100%;
    }
</style>
@endsection

@section('content')
    <div class="container standalone">
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/carousel">캐러셀 이미지</a></li>
            <li class="active">이미지 추가</li>
        </ol>

        <form action="/carousel" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-sm-6 col-md-8">
                    <div class="form-group {{ ($errors->has('new_image')) ? 'has-error' : '' }}">
                        <label for="new_image_field" class="control-label">새 이미지 추가 <small></small></label>
                        <input id="new_image_field" name="new_image" type="file" class="form-control" value="{{ old('new_image') }}">
                        <span class="help-block"><small>{{$errors->first('new_image')}}</small></span>
                    </div>
                    <button type="submit" class="btn btn-primary">추가</button>
                </div>
                <div class="col-sm-6 col-md-4">
                    <img src="#" alt="이미지 미리보기" id="new_image_preview" class="thumbnail narrow">
                </div>
            </div>
        </form>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(function () {
            $('input[type=file]').change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    var prev = $('#'+this.name+'_preview');
                    reader.onload = function (e) {
                        prev.attr('src', e.target.result);
                    }
                    reader.readAsDataURL(this.files[0]);
                }
            });
        });
    </script>
@endsection