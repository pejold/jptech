@extends('layout')

@section('title', ':: 가입신청')

@section('content')
    <div class="container standalone">
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li class="active">가입신청</li>
        </ol>
        <div class="page-header">
            <h2>가입신청 <small></small></h2>
        </div>
        <div class="alert alert-success">
            가입신청이 성공적으로 완료되었습니다.
        </div>
    </div>
@endsection