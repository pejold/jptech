@extends('layout')

@section('title', ':: 신청서 수정')

@section('content')
    <div class="container standalone">
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/application">신청현황</a></li>
            <li><a href="/application/{{$application->id}}">{{$application->name}}</a></li>
            <li class="active">수정</li>
        </ol>
        <div class="page-header">
            <h2>신청서 수정 <small></small></h2>
        </div>
        <form action="/application/{{$application->id}}" method="POST">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <h4>신청상품 정보</h4>
            <table class="table form-table">
                <tr class="form-group @if ($errors->has('product')) has-error @endif">
                    <th><label for="product_field" class="control-label required">라우터</label></th>
                    <td>
                        <select name="product" id="product_field" required="required" class="form-control">
                            <option value="">라우터를 선택하세요</option>
                            <option {{ (old('product') == 'LFM-100') || (!old('product') && $application->product == 'LFM-100') ? 'selected="selected"' : '' }} value="LFM-100">LFM-100</option>
                            <option {{ (old('product') == 'ME-Y30K') || (!old('product') && $application->product == 'ME-Y30K') ? 'selected="selected"' : '' }} value="ME-Y30K">ME-Y30K</option>
                            <option {{ (old('product') == 'HS-2300') || (!old('product') && $application->product == 'HS-2300') ? 'selected="selected"' : '' }} value="HS-2300">HS-2300</option>
                        </select>
                        <span class="help-block">{{$errors->first('product')}}</span>
                    </td>
                </tr>
                <tr class="form-group @if ($errors->has('plan')) has-error @endif">
                    <th><label for="plan_field" class="control-label required">요금제</label></th>
                    <td>
                        <select name="plan" id="plan_field" required="required" class="form-control">
                            <option value="">요금제를 선택하세요</option>
                            <option {{ (old('plan') == '10G') || (!old('plan') && $application->plan == '10G') ? 'selected="selected"' : '' }} value="10G">LTE라우터 10G 월 기본료 15,000원(VAT 별도)</option>
                            <option {{ (old('plan') == '20G') || (!old('plan') && $application->plan == '20G') ? 'selected="selected"' : '' }} value="20G">LTE라우터 20G 월 기본료 22,500원(VAT 별도)</option>
                        </select>
                        <span class="help-block">{{$errors->first('plan')}}</span>
                    </td>
                </tr>
                <tr class="form-group @if ($errors->has('contract')) has-error @endif">
                    <th><label class="control-label required">약정기간</label></th>
                    <td>
                        <div class="radio">
                            <label><input name="contract" type="radio" value="2Y" required="required" {{ (old('contract') == '2Y') || (!old('contract') && $application->contract == '2Y') ? 'checked="checked"' : '' }}> 2년</label>
                        </div>
                        <span class="help-block">{{$errors->first('contract')}}</span>
                    </td>
                </tr>
            </table>
            <h4>가입자 정보</h4>
            <table class="table form-table">
                <tr class="form-group @if ($errors->has('customer_type')) has-error @endif">
                    <th><label class="control-label required">고객구분</label></th>
                    <td>
                        <div class="radio">
                            <label><input name="customer_type" type="radio" value="personal" required="required" {{ (old('customer_type') == 'personal') || (!old('customer_type') && $application->customer_type == 'personal') ? 'checked="checked"' : '' }}> 개인</label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <label><input name="customer_type" type="radio" value="business" required="required" {{ (old('customer_type') == 'business') || (!old('customer_type') && $application->customer_type == 'business') ? 'checked="checked"' : '' }}> 법인(개인) 사업자</label>
                        </div>
                        <span class="help-block">{{$errors->first('customer_type')}}</span>
                    </td>
                </tr>
                <tr class="form-group @if ($errors->has('name')) has-error @endif">
                    <th><label for="name_field" class="control-label required">이름(회사명)</label></th>
                    <td>
                        <input id="name_field" name="name" type="text" required="required" value="{{ old('name')?:$application->name }}" class="form-control">
                        <span class="help-block">{{$errors->first('name')}}</span>
                    </td>
                </tr>
                <tr class="form-group @if ($errors->has('telephone')) has-error @endif">
                    <th><label for="telephone_field" class="control-label">전화번호</label></th>
                    <td>
                        <input id="telephone_field" name="telephone" type="tel" value="{{ old('telephone')?:$application->telephone }}" class="form-control">
                        <span class="help-block">{{$errors->first('telephone')}}</span>
                    </td>
                </tr>
                <tr class="form-group @if ($errors->has('mobile')) has-error @endif">
                    <th><label for="mobile_field" class="control-label required">핸드폰번호</label></th>
                    <td>
                        <input id="mobile_field" name="mobile" type="tel" required="required" value="{{ old('mobile')?:$application->mobile }}" class="form-control">
                        <span class="help-block">{{$errors->first('mobile')}}</span>
                    </td>
                </tr>
                <tr class="form-group @if ($errors->has('postcode') or $errors->has('address') or $errors->has('address_detail')) has-error @endif">
                    <th><label for="postcode_field" class="control-label required">배송받을 주소</label></th>
                    <td>
                        <div class="input-group">
                            <input placeholder="우편번호" id="postcode_field" name="postcode" type="text" readonly="readonly" required="required" value="{{ old('postcode')?:$application->postcode }}" class="form-control">
                            <span class="input-group-btn"><button class="btn btn-default" type="button" onclick="openPostcode()">우편번호 검색</button></span>
                        </div>
                        <span class="help-block">{{$errors->first('postcode')}}</span>
                        <input placeholder="주소" id="address_field" name="address" type="text" readonly="readonly" required="required" value="{{ old('address')?:$application->address }}" class="form-control">
                        <span class="help-block">{{$errors->first('address')}}</span>
                        <input placeholder="상세주소" id="address_detail_field" name="address_detail" type="text" required="required" value="{{ old('address_detail')?:$application->address_detail }}" class="form-control">
                        <span class="help-block">{{$errors->first('address_detail')}}</span>
                    </td>
                </tr>
            </table>
            <h4>추천인</h4>
            <table class="table form-table">
                <tr class="form-group @if ($errors->has('recommander')) has-error @endif">
                    <th><label for="recommander_field" class="control-label">추천인</label></th>
                    <td>
                        <input placeholder="구매에 도움을 주신분이 있으면 기재해주세요" id="recommander_field" name="recommander" type="text" value="{{ old('recommander')?:$application->recommander }}" class="form-control">
                        <span class="help-block">{{$errors->first('recommander')}}</span>
                    </td>
                </tr>
            </table>
            <h4>요금납부 방식</h4>
            <table class="table form-table">
                <tr class="form-group @if ($errors->has('payment_type')) has-error @endif">
                    <th><label for="payment_type_field" class="control-label">결제방식</label></th>
                    <td>
                        <div class="radio">
                            <label><input name="payment_type" type="radio" value="card" {{ (old('payment_type') == 'card') || (!old('payment_type') && $application->payment_type == 'card') ? 'checked="checked"' : '' }}> 신용카드</label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <label><input name="payment_type" type="radio" value="bank" {{ (old('payment_type') == 'bank') || (!old('payment_type') && $application->payment_type == 'bank') ? 'checked="checked"' : '' }}> 계좌이체</label>
                        </div>
                        <span class="help-block">{{$errors->first('payment_type')}}</span>
                    </td>
                </tr>
                <tr class="card_row form-group @if ($errors->has('card_type')) has-error @endif">
                    <th><label for="card_type_field" class="control-label">카드사</label></th>
                    <td>
                        <select id="card_type_field" name="card_type" class="form-control">
                            <option value=""></option>
                            <option {{ (old('card_type') == '국민카드') || (!old('card_type') && $application->card_type == '국민카드') ? 'selected="selected"' : '' }} value="국민카드">국민카드</option>
                            <option {{ (old('card_type') == '현대카드') || (!old('card_type') && $application->card_type == '현대카드') ? 'selected="selected"' : '' }} value="현대카드">현대카드</option>
                            <option {{ (old('card_type') == 'BC카드') || (!old('card_type') && $application->card_type == 'BC카드') ? 'selected="selected"' : '' }} value="BC카드">BC카드</option>
                            <option {{ (old('card_type') == '외환카드') || (!old('card_type') && $application->card_type == '외환카드') ? 'selected="selected"' : '' }} value="외환카드">외환카드</option>
                            <option {{ (old('card_type') == '삼성카드') || (!old('card_type') && $application->card_type == '삼성카드') ? 'selected="selected"' : '' }} value="삼성카드">삼성카드</option>
                            <option {{ (old('card_type') == '롯데카드') || (!old('card_type') && $application->card_type == '롯데카드') ? 'selected="selected"' : '' }} value="롯데카드">롯데카드</option>
                            <option {{ (old('card_type') == '신한카드') || (!old('card_type') && $application->card_type == '신한카드') ? 'selected="selected"' : '' }} value="신한카드">신한카드</option>
                            <option {{ (old('card_type') == 'NH카드') || (!old('card_type') && $application->card_type == 'NH카드') ? 'selected="selected"' : '' }} value="NH카드">NH카드</option>
                        </select>
                        <span class="help-block">{{$errors->first('card_type')}}</span>
                    </td>
                </tr>
                <tr class="card_row form-group @if ($errors->has('card_number')) has-error @endif">
                    <th><label for="card_number_field" class="control-label">카드번호</label></th>
                    <td>
                        <input id="card_number_field" name="card_number" type="text" value="{{ old('card_number')?:$application->card_number }}" class="form-control">
                        <span class="help-block">{{$errors->first('card_number')}}</span>
                    </td>
                </tr>
                <tr class="card_row form-group @if ($errors->has('expire_year') or $errors->has('expire_month')) has-error @endif">
                    <th><label for="expire_year_field" class="control-label">유효기간</label></th>
                    <td>
                        <div class="row">
                            <div class="col-sm-6">
                                <input placeholder="년도" id="expire_year_field" name="expire_year" type="number" min="2017" max="2100" minlength="4" maxlength="4" value="{{ old('expire_year')?:$application->expire_year }}" class="form-control" />
                                <span class="help-block">{{$errors->first('expire_year')}}</span>
                            </div>
                            <div class="col-sm-6">
                                <input placeholder="월" id="expire_month_field" name="expire_month" type="number" min="1" max="12" minlength="1" maxlength="2" value="{{ old('expire_month')?:$application->expire_month }}" class="form-control" />
                                <span class="help-block">{{$errors->first('expire_month')}}</span>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="bank_row form-group @if ($errors->has('bank_name')) has-error @endif">
                    <th><label for="bank_name_field" class="control-label">계좌은행</label></th>
                    <td>
                        <select id="bank_name_field" name="bank_name" class="form-control">
                            <option value=""></option>
                            <option {{ (old('bank_name') == '국민은행') || (!old('bank_name') && $application->bank_name == '국민은행') ? 'selected="selected"' : '' }} value="국민은행">국민은행</option>
                            <option {{ (old('bank_name') == '신한은행') || (!old('bank_name') && $application->bank_name == '신한은행') ? 'selected="selected"' : '' }} value="신한은행">신한은행</option>
                            <option {{ (old('bank_name') == '하나은행') || (!old('bank_name') && $application->bank_name == '하나은행') ? 'selected="selected"' : '' }} value="하나은행">하나은행</option>
                            <option {{ (old('bank_name') == '우리은행') || (!old('bank_name') && $application->bank_name == '우리은행') ? 'selected="selected"' : '' }} value="우리은행">우리은행</option>
                            <option {{ (old('bank_name') == '외환은행') || (!old('bank_name') && $application->bank_name == '외환은행') ? 'selected="selected"' : '' }} value="외환은행">외환은행</option>
                            <option {{ (old('bank_name') == '씨티은행') || (!old('bank_name') && $application->bank_name == '씨티은행') ? 'selected="selected"' : '' }} value="씨티은행">씨티은행</option>
                            <option {{ (old('bank_name') == 'IBK기업은행') || (!old('bank_name') && $application->bank_name == 'IBK기업은행') ? 'selected="selected"' : '' }} value="IBK기업은행">IBK기업은행</option>
                            <option {{ (old('bank_name') == '농협중앙회') || (!old('bank_name') && $application->bank_name == '농협중앙회') ? 'selected="selected"' : '' }} value="농협중앙회">농협중앙회</option>
                            <option {{ (old('bank_name') == '전북은행') || (!old('bank_name') && $application->bank_name == '전북은행') ? 'selected="selected"' : '' }} value="전북은행">전북은행</option>
                            <option {{ (old('bank_name') == '광주은행') || (!old('bank_name') && $application->bank_name == '광주은행') ? 'selected="selected"' : '' }} value="광주은행">광주은행</option>
                            <option {{ (old('bank_name') == '대구은행') || (!old('bank_name') && $application->bank_name == '대구은행') ? 'selected="selected"' : '' }} value="대구은행">대구은행</option>
                            <option {{ (old('bank_name') == '경남은행') || (!old('bank_name') && $application->bank_name == '경남은행') ? 'selected="selected"' : '' }} value="경남은행">경남은행</option>
                            <option {{ (old('bank_name') == '부산은행') || (!old('bank_name') && $application->bank_name == '부산은행') ? 'selected="selected"' : '' }} value="부산은행">부산은행</option>
                            <option {{ (old('bank_name') == '제주은행') || (!old('bank_name') && $application->bank_name == '제주은행') ? 'selected="selected"' : '' }} value="제주은행">제주은행</option>
                            <option {{ (old('bank_name') == 'KDB산업은행') || (!old('bank_name') && $application->bank_name == 'KDB산업은행') ? 'selected="selected"' : '' }} value="KDB산업은행">KDB산업은행</option>
                            <option {{ (old('bank_name') == 'SC은행') || (!old('bank_name') && $application->bank_name == 'SC은행') ? 'selected="selected"' : '' }} value="SC은행">SC은행</option>
                            <option {{ (old('bank_name') == '새마을금고') || (!old('bank_name') && $application->bank_name == '새마을금고') ? 'selected="selected"' : '' }} value="새마을금고">새마을금고</option>
                            <option {{ (old('bank_name') == '신협은행') || (!old('bank_name') && $application->bank_name == '신협은행') ? 'selected="selected"' : '' }} value="신협은행">신협은행</option>
                        </select>
                        <span class="help-block">{{$errors->first('bank_name')}}</span>
                    </td>
                </tr>
                <tr class="bank_row form-group @if ($errors->has('bank_account_number')) has-error @endif">
                    <th><label for="bank_account_number_field" class="control-label">계좌번호</label></th>
                    <td>
                        <input id="bank_account_number_field" name="bank_account_number" type="number" value="{{ old('bank_account_number')?:$application->bank_account_number }}" class="form-control">
                        <span class="help-block">{{$errors->first('bank_account_number')}}</span>
                    </td>
                </tr>
            </table>
            <h4>기타 요청사항</h4>
            <table class="table form-table">
                <tr class="form-group @if ($errors->has('memo')) has-error @endif">
                    <th><label for="memo_field" class="control-label">기타요청사항</label></th>
                    <td>
                        <textarea name="memo" id="memo_field" rows="10" class="form-control">{{ old('memo')?:$application->memo }}</textarea>
                        <span class="help-block">{{$errors->first('memo')}}</span>
                    </td>
                </tr>
            </table>
            <div class="text-right">
                <a href="/application/{{$application->id}}" class="btn btn-default">취소</a>
                <button class="btn btn-primary" type="submit">수정</button>
            </div>
        </form>
    </div>
@endsection

@section('script')
    <script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
    <script src="/js/application.js"></script>
@endsection