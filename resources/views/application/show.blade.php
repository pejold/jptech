@extends('layout')

@section('title', ":: $application->name")

@section('style')
    <style type="text/css">
        .panel {
            margin-bottom: 10px!important;
        }
        th {
            width: 90px;
        }
        .row {
            margin-left: -5px;
            margin-right: -5px;
        }
    </style>
@endsection

@section('content')
    <div class="container standalone">
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/application">신청현황</a></li>
            <li class="active">{{$application->name}}</li>
        </ol>
        <div class="page-header">
            <div class="pull-right">
                <form id="status_form" action="/application/{{$application->id}}/status" method="POST" enctype="application/x-www-form-urlencoded">
                    {{csrf_field()}}
                    {{method_field('PUT')}}
                    <input type="hidden" name="status" id="status_field" />
                    <div class="dropdown">
                        <button type="button" class="dropdown-toggle btn btn-sm btn-{{$statusColors[$application->status]}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            {{$statusLabels[$application->status]}}
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            @foreach ($statusLabels as $key => $label)
                                @continue($key == $application->status)
                            <li><a href="#" data-status="{{$key}}" class="small text-{{$statusColors[$key]}}" onclick="changeStatus(this)">{{$label}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </form>
            </div>
            <h2>가입신청 내역 <small>{{$application->created_at}}</small></h2>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-7">
                <div class="panel panel-default">
                    <div class="panel-heading">가입자 정보</div>
                    <table class="table small">
                        <tr>
                            <th>고객구분</th>
                            <td>{{$application->customer_type == 'personal' ? '개인고객' : '사업자고객'}}</td>
                        </tr>
                        <tr>
                            <th>이름(회사명)</th>
                            <td>{{$application->name}}</td>
                        </tr>
                        <tr>
                            <th>전화번호</th>
                            <td>{{format_phone($application->telephone)}}</td>
                        </tr>
                        <tr>
                            <th>핸드폰번호</th>
                            <td>{{format_phone($application->mobile)}}</td>
                        </tr>
                        <tr>
                            <th>배송받을 주소</th>
                            <td>
                                <p>우편번호 : {{$application->postcode}}</p>
                                <p>{{$application->address}} {{$application->address_detail}}</p>
                            </td>
                        </tr>
                        <tr>
                            <th>추천인</th>
                            <td>{{$application->recommander}}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-sm-6 col-md-5">
                <div class="panel panel-default">
                    <div class="panel-heading">신청상품 정보</div>
                    <table class="table small">
                        <tr>
                            <th>라우터</th>
                            <td>{{$application->product}}</td>
                        </tr>
                        <tr>
                            <th>요금제</th>
                            <td>{{$application->plan}}</td>
                        </tr>
                        <tr>
                            <th>약정기간</th>
                            <td>2년</td>
                        </tr>
                    </table>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">요금납부 방식</div>
                    <table class="table small">
                        @if ($application->payment_type == 'card')
                        <tr>
                            <th>결제방식</th>
                            <td>카드결제</td>
                        </tr>
                        <tr class="card_row form-group @if ($errors->has('card_type')) has-error @endif">
                            <th>카드사</th>
                            <td>{{$application->card_type}}</td>
                        </tr>
                        <tr class="card_row form-group @if ($errors->has('card_number')) has-error @endif">
                            <th>카드번호</th>
                            <td>{{$application->card_number}}</td>
                        </tr>
                        <tr class="card_row form-group @if ($errors->has('expire_year') or $errors->has('expire_month')) has-error @endif">
                            <th>유효기간</th>
                            <td>{{$application->expire_date}}</td>
                        </tr>
                        @elseif ($application->payment_type == 'bank')
                        <tr>
                            <th>결제방식</th>
                            <td>계좌이체</td>
                        </tr>
                        <tr class="bank_row form-group @if ($errors->has('bank_name')) has-error @endif">
                            <th>계좌은행</th>
                            <td>{{$application->bank_name}}</td>
                        </tr>
                        <tr class="bank_row form-group @if ($errors->has('bank_account_number')) has-error @endif">
                            <th>계좌번호</th>
                            <td>{{$application->bank_account_number}}</td>
                        </tr>
                        @endif
                    </table>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">기타 요청사항</div>
                    <div class="panel-body">{!! nl2br($application->memo) !!}</div>
                </div>
            </div>
        </div>
        <div class="text-right">
            @if ($application->status == 'waiting' || $application->status == 'accepted')
            <form id="delete_form" action="/application/{{$application->id}}" method="POST">
                {{csrf_field()}}
                {{method_field('DELETE')}}
            </form>
            <button type="button" class="btn btn-danger" onclick="deleteApplication()">삭제</button>
            <a href="/application/{{$application->id}}/edit" class="btn btn-default">수정</a>
            @endif
            <a href="/application" class="btn btn-default">목록보기</a>
        </div>
    </div>
@endsection

@section('script')
    <script>
        function changeStatus(element) {
            $('#status_field').val($(element).attr('data-status'));
            $('#status_form').submit();
        }

        function deleteApplication() {
            if (confirm('삭제된 신청서는 복원할 수 없습니다.\n정말 삭제하겠습니까?')) {
                $('#delete_form').submit();
            }
        }
    </script>
@endsection