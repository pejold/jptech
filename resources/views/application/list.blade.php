@extends('layout')

@section('title', ':: 신청현황')

@section('content')
    <div class="container standalone">
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li class="active">신청현황</li>
        </ol>
        <div class="page-header">
            <h2>신청현황 <small>:: 총 {{$applications->total()}} 건</small></h2>
        </div>
        <form action="/application" method="GET">
            <div class="pull-right search-bar input-group input-group-sm">
                <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                <input name="keyword" type="text" class="form-control" value="{{request('keyword')}}" placeholder="핸드폰 뒷자리">
                <span class="input-group-btn">
                    <button class="btn btn-primary" type="submit">검색</button>
                </span>
            </div>
        </form>

        <table class="table">
            <thead>
                <tr>
                    <th class="text-center">순번</th>
                    <th class="text-center">이름(회사명)</th>
                    <th class="text-center">핸드폰번호 뒷자리</th>
                    <th class="text-center">라우터</th>
                    <th class="text-center">요금제</th>
                    <th class="text-center">신청일</th>
                    <th class="text-center">상태</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($applications as $application)
                <tr>
                    <td class="text-center">{{$application->id}}</td>
                    @auth
                    <td class="text-center"><a href="/application/{{$application->id}}">{{$application->name}}</a></td>
                    @else
                    <td class="text-center text-primary">{{$application->name}}</td>
                    @endauth
                    <td class="text-center">{{substr($application->mobile, -4)}}</td>
                    <td class="text-center">{{$application->product}}</td>
                    <td class="text-center">{{$application->plan}}</td>
                    <td class="text-center">{{$application->created_at->toDateString()}}</td>
                    <td class="text-center">
                        <span class="label label-{{$statusColors[$application->status]}}">{{$statusLabels[$application->status]}}</span>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="7" class="text-center placeholder">등록된 신청내역이 없습니다.</td>
                </tr>
                @endforelse
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="7" class="text-center">
                        <a href="/application/create" class="btn btn-default pull-right">가입신청</a>
                        {{ $applications->appends(['keyword'=>request('keyword')])->links() }}
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
@endsection