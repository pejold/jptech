@extends('layout')

@section('title', ':: 가입신청')

@section('style')
    <link rel="stylesheet" href="/js/components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
    <style type="text/css">
        .datetime-help-block {
            margin-top: 36px;
        }
    </style>
@endsection

@section('content')
    <div class="container standalone">
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li class="active">가입신청</li>
        </ol>
        <div class="page-header">
            <h2>가입신청 <small></small></h2>
        </div>
        <form action="/application" method="POST">
            {{ csrf_field() }}
            @if ($errors->any())
            <div class="alert alert-danger">
                <p>작성내용 중 올바르지 않은 내용이 있습니다.</p>
                <p>해당 항목에서 상세 내용을 확인하세요.</p>
                <p>{{ print_r($errors) }}</p>
            </div>
            <p></p>   
            @endif
            <div class="alert alert-warning">
                <h4>신청시 유의사항</h4>
                <ol>
                    <li>
                        아래 신청서는 간편신청서이며, 신청 후 1시간 이내에 개통담당 직원이 유선상으로 개통관련 최종확인을 하오니 필히 <strong>031-309-0550</strong> 번의 전화를 받아주셔야합니다.
                        <ul>
                            <li>주말이나 휴무일은 익월 9시~10시경에 유선상으로 개통지원을 해드립니다.</li>
                        </ul>
                    </li>
                    <li>개통지원시 가입고객 본인의 신분증사본을 팩스 또는 문자등으로 보내주셔야 합니다.</li>
                    <li>
                        해당기기는 모바일데이타상품으로 약정기간은 2년이며 약정기간내 해지시 별도의 약정위약금이 발생합니다. 
                        <ul>
                            <li>
                                위약금은 사용개월수에 따라 자동 감액됩니다.
                                <ul>
                                    <li>LFM-100 : 141,900원</li>
                                    <li>ME-Y30K : 141,900원</li>
                                    <li>HS-2300 : 101,200원</li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li>가입일로부터 93일 이후 일시정지/요금제변경/해지 등이 가능하며, 일시정지는 2회/1년 만 가능하며 최고 180일까지만 가능합니다.</li>
                    <li>최초개통일이 월중일 경우 해당월 잔여일 일할계산으로 데이터가제공됩니다.</li>
                    <li>장기연체나 신용불량,외국인인 경우 개통이 불가하며, 단, 통신회선수 부족시 회선추가요청을 통해서 개통가능합니다.</li>
                </ol>
            </div>
            @auth
            <h4>임의 신청일시</h4>
            <table class="table form-table">
                <tr class="form-group @if ($errors->has('created_at')) has-error @endif">
                    <th><label for="datetime_field" class="control-label required">작성일시</label></th>
                    <td>
                        <div class="row">
                            <div class="col-xs-12">
                                <input id="datetime_field" placeholder="년도-월-일 시:분" type='text' name="created_at" value="{{ old('created_at') }}" class="form-control" />
                            </div>
                        </div>
                        <span class="help-block"><small>{{$errors->first('created_at')}}</small></span>
                    </td>
                </tr>
            </table>
            @endauth
            <h4>신청상품 정보</h4>
            <table class="table form-table">
                <tr class="form-group @if ($errors->has('product')) has-error @endif">
                    <th><label for="product_field" class="control-label required">라우터</label></th>
                    <td>
                        <select name="product" id="product_field" required="required" class="form-control">
                            <option value="">라우터를 선택하세요</option>
                            <option {{ (old('product') == 'LFM-100') ? 'selected="selected"' : '' }} value="LFM-100">LFM-100</option>
                            <option {{ (old('product') == 'ME-Y30K') ? 'selected="selected"' : '' }} value="ME-Y30K">ME-Y30K</option>
                            <option {{ (old('product') == 'HS-2300') ? 'selected="selected"' : '' }} value="HS-2300">HS-2300</option>
                        </select>
                        <span class="help-block"><small>{{$errors->first('product')}}</small></span>
                    </td>
                </tr>
                <tr class="form-group @if ($errors->has('plan')) has-error @endif">
                    <th><label for="plan_field" class="control-label required">요금제</label></th>
                    <td>
                        <select name="plan" id="plan_field" required="required" class="form-control">
                            <option value="">요금제를 선택하세요</option>
                            <option {{ (old('plan') == '10G') ? 'selected="selected"' : '' }} value="10G">LTE라우터 10G 월 기본료 15,000원(VAT 별도)</option>
                            <option {{ (old('plan') == '20G') ? 'selected="selected"' : '' }} value="20G">LTE라우터 20G 월 기본료 22,500원(VAT 별도)</option>
                        </select>
                        <span class="help-block"><small>{{$errors->first('plan')}}</small></span>
                    </td>
                </tr>
                <tr class="form-group @if ($errors->has('contract')) has-error @endif">
                    <th><label class="control-label required">약정기간</label></th>
                    <td>
                        <div class="radio">
                            <label><input name="contract" type="radio" value="2Y" required="required" {{ (old('contract') == '2Y') ? 'checked="checked"' : '' }}> 2년</label>
                        </div>
                        <span class="help-block"><small>{{$errors->first('contract')}}</small></span>
                    </td>
                </tr>
            </table>
            <h4>가입자 정보</h4>
            <table class="table form-table">
                <tr class="form-group @if ($errors->has('customer_type')) has-error @endif">
                    <th><label class="control-label required">고객구분</label></th>
                    <td>
                        <div class="radio">
                            <label><input name="customer_type" type="radio" value="personal" required="required" {{ (old('customer_type') == 'personal') ? 'checked="checked"' : '' }}> 개인</label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <label><input name="customer_type" type="radio" value="business" required="required" {{ (old('customer_type') == 'business') ? 'checked="checked"' : '' }}> 법인(개인) 사업자</label>
                        </div>
                        <span class="help-block"><small>{{$errors->first('customer_type')}}</small></span>
                    </td>
                </tr>
                <tr class="form-group @if ($errors->has('name')) has-error @endif">
                    <th><label for="name_field" class="control-label required">이름(회사명)</label></th>
                    <td>
                        <input id="name_field" name="name" type="text" required="required" value="{{ old('name') }}" class="form-control">
                        <span class="help-block"><small>{{$errors->first('name')}}</small></span>
                    </td>
                </tr>
                <tr class="form-group @if ($errors->has('telephone')) has-error @endif">
                    <th><label for="telephone_field" class="control-label">전화번호</label></th>
                    <td>
                        <input id="telephone_field" name="telephone" type="tel" value="{{ old('telephone') }}" class="form-control">
                        <span class="help-block"><small>{{$errors->first('telephone')}}</small></span>
                    </td>
                </tr>
                <tr class="form-group @if ($errors->has('mobile')) has-error @endif">
                    <th><label for="mobile_field" class="control-label required">핸드폰번호</label></th>
                    <td>
                        <input id="mobile_field" name="mobile" type="tel" required="required" value="{{ old('mobile') }}" class="form-control">
                        <span class="help-block"><small>{{$errors->first('mobile')}}</small></span>
                    </td>
                </tr>
                <tr class="form-group @if ($errors->has('postcode') or $errors->has('address') or $errors->has('address_detail')) has-error @endif">
                    <th><label for="postcode_field" class="control-label required">배송받을 주소</label></th>
                    <td>
                        <div class="input-group">
                            <input placeholder="우편번호" id="postcode_field" name="postcode" type="text" readonly="readonly" required="required" value="{{ old('postcode') }}" class="form-control">
                            <span class="input-group-btn"><button class="btn btn-default" type="button" onclick="openPostcode()">우편번호 검색</button></span>
                        </div>
                        <span class="help-block"><small>{{$errors->first('postcode')}}</small></span>
                        <input placeholder="주소" id="address_field" name="address" type="text" readonly="readonly" required="required" value="{{ old('address') }}" class="form-control">
                        <span class="help-block"><small>{{$errors->first('address')}}</small></span>
                        <input placeholder="상세주소" id="address_detail_field" name="address_detail" type="text" required="required" value="{{ old('address_detail') }}" class="form-control">
                        <span class="help-block"><small>{{$errors->first('address_detail')}}</small></span>
                    </td>
                </tr>
            </table>
            <h4>추천인</h4>
            <table class="table form-table">
                <tr class="form-group @if ($errors->has('recommander')) has-error @endif">
                    <th><label for="recommander_field" class="control-label">추천인</label></th>
                    <td>
                        <input placeholder="구매에 도움을 주신분이 있으면 기재해주세요" id="recommander_field" name="recommander" type="text" value="{{old('recommander')}}" class="form-control">
                        <span class="help-block"><small>{{$errors->first('recommander')}}</small></span>
                    </td>
                </tr>
            </table>
            <h4>요금납부 방식</h4>
            <table class="table form-table">
                <tr class="form-group @if ($errors->has('payment_type')) has-error @endif">
                    <th><label for="payment_type_field" class="control-label">결제방식</label></th>
                    <td>
                        <div class="radio">
                            <label><input name="payment_type" type="radio" value="card" {{ (old('payment_type') == 'card') ? 'checked="checked"' : '' }}> 신용카드</label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <label><input name="payment_type" type="radio" value="bank" {{ (old('payment_type') == 'bank') ? 'checked="checked"' : '' }}> 계좌이체</label>
                        </div>
                        <span class="help-block"><small>{{$errors->first('payment_type')}}</small></span>
                    </td>
                </tr>
                <tr class="card_row form-group @if ($errors->has('card_type')) has-error @endif">
                    <th><label for="card_type_field" class="control-label">카드사</label></th>
                    <td>
                        <select id="card_type_field" name="card_type" class="form-control">
                            <option value=""></option>
                            <option {{ (old('card_type') == '국민카드') ? 'selected="selected"' : '' }} value="국민카드">국민카드</option>
                            <option {{ (old('card_type') == '현대카드') ? 'selected="selected"' : '' }} value="현대카드">현대카드</option>
                            <option {{ (old('card_type') == 'BC카드') ? 'selected="selected"' : '' }} value="BC카드">BC카드</option>
                            <option {{ (old('card_type') == '외환카드') ? 'selected="selected"' : '' }} value="외환카드">외환카드</option>
                            <option {{ (old('card_type') == '삼성카드') ? 'selected="selected"' : '' }} value="삼성카드">삼성카드</option>
                            <option {{ (old('card_type') == '롯데카드') ? 'selected="selected"' : '' }} value="롯데카드">롯데카드</option>
                            <option {{ (old('card_type') == '신한카드') ? 'selected="selected"' : '' }} value="신한카드">신한카드</option>
                            <option {{ (old('card_type') == 'NH카드') ? 'selected="selected"' : '' }} value="NH카드">NH카드</option>
                        </select>
                        <span class="help-block"><small>{{$errors->first('card_type')}}</small></span>
                    </td>
                </tr>
                <tr class="card_row form-group @if ($errors->has('card_number')) has-error @endif">
                    <th><label for="card_number_field" class="control-label">카드번호</label></th>
                    <td>
                        <input id="card_number_field" name="card_number" type="text" value="{{old('card_number')}}" class="form-control">
                        <span class="help-block"><small>{{$errors->first('card_number')}}</small></span>
                    </td>
                </tr>
                <tr class="card_row form-group @if ($errors->has('expire_year') or $errors->has('expire_month')) has-error @endif">
                    <th><label for="expire_year_field" class="control-label">유효기간</label></th>
                    <td>
                        <div class="row">
                            <div class="col-sm-6">
                                <input placeholder="년도" id="expire_year_field" name="expire_year" type="number" min="2017" max="2100" minlength="4" maxlength="4" value="{{old('expire_year')}}" class="form-control" />
                                <span class="help-block"><small>{{$errors->first('expire_year')}}</small></span>
                            </div>
                            <div class="col-sm-6">
                                <input placeholder="월" id="expire_month_field" name="expire_month" type="number" min="1" max="12" minlength="1" maxlength="2" value="{{old('expire_month')}}" class="form-control" />
                                <span class="help-block"><small>{{$errors->first('expire_month')}}</small></span>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="bank_row form-group @if ($errors->has('bank_name')) has-error @endif">
                    <th><label for="bank_name_field" class="control-label">계좌은행</label></th>
                    <td>
                        <select id="bank_name_field" name="bank_name" class="form-control">
                            <option value=""></option>
                            <option {{ (old('bank_name') == '국민은행') ? 'selected="selected"' : '' }} value="국민은행">국민은행</option>
                            <option {{ (old('bank_name') == '신한은행') ? 'selected="selected"' : '' }} value="신한은행">신한은행</option>
                            <option {{ (old('bank_name') == '하나은행') ? 'selected="selected"' : '' }} value="하나은행">하나은행</option>
                            <option {{ (old('bank_name') == '우리은행') ? 'selected="selected"' : '' }} value="우리은행">우리은행</option>
                            <option {{ (old('bank_name') == '외환은행') ? 'selected="selected"' : '' }} value="외환은행">외환은행</option>
                            <option {{ (old('bank_name') == '씨티은행') ? 'selected="selected"' : '' }} value="씨티은행">씨티은행</option>
                            <option {{ (old('bank_name') == 'IBK기업은행') ? 'selected="selected"' : '' }} value="IBK기업은행">IBK기업은행</option>
                            <option {{ (old('bank_name') == '농협중앙회') ? 'selected="selected"' : '' }} value="농협중앙회">농협중앙회</option>
                            <option {{ (old('bank_name') == '전북은행') ? 'selected="selected"' : '' }} value="전북은행">전북은행</option>
                            <option {{ (old('bank_name') == '광주은행') ? 'selected="selected"' : '' }} value="광주은행">광주은행</option>
                            <option {{ (old('bank_name') == '대구은행') ? 'selected="selected"' : '' }} value="대구은행">대구은행</option>
                            <option {{ (old('bank_name') == '경남은행') ? 'selected="selected"' : '' }} value="경남은행">경남은행</option>
                            <option {{ (old('bank_name') == '부산은행') ? 'selected="selected"' : '' }} value="부산은행">부산은행</option>
                            <option {{ (old('bank_name') == '제주은행') ? 'selected="selected"' : '' }} value="제주은행">제주은행</option>
                            <option {{ (old('bank_name') == 'KDB산업은행') ? 'selected="selected"' : '' }} value="KDB산업은행">KDB산업은행</option>
                            <option {{ (old('bank_name') == 'SC은행') ? 'selected="selected"' : '' }} value="SC은행">SC은행</option>
                            <option {{ (old('bank_name') == '새마을금고') ? 'selected="selected"' : '' }} value="새마을금고">새마을금고</option>
                            <option {{ (old('bank_name') == '신협은행') ? 'selected="selected"' : '' }} value="신협은행">신협은행</option>
                        </select>
                        <span class="help-block"><small>{{$errors->first('bank_name')}}</small></span>
                    </td>
                </tr>
                <tr class="bank_row form-group @if ($errors->has('bank_account_number')) has-error @endif">
                    <th><label for="bank_account_number_field" class="control-label">계좌번호</label></th>
                    <td>
                        <input id="bank_account_number_field" name="bank_account_number" type="number" value="{{old('bank_account_number')}}" placeholder="숫자만 입력해주세요" class="form-control">
                        <span class="help-block"><small>{{$errors->first('bank_account_number')}}</small></span>
                    </td>
                </tr>
            </table>
            <h4>기타 요청사항</h4>
            <table class="table form-table">
                <tr class="form-group @if ($errors->has('memo')) has-error @endif">
                    <th><label for="memo_field" class="control-label">기타요청사항</label></th>
                    <td>
                        <textarea name="memo" id="memo_field" rows="10" class="form-control">{{old('memo')}}</textarea>
                        <span class="help-block"><small>{{$errors->first('memo')}}</small></span>
                    </td>
                </tr>
            </table>
            <div class="alert alert-info">
                <h4>개인정보 수집 및 이용에 대한 안내</h4>
                <p>당사는 고객님의 개인정보를 중요시하며, "정보통신망 이용촉진 및 정보보호"에 관한 법률을 준수하고 개인정보취급방침을 통하여 고객님께서 제공하시는 개인정보가 어떠한 용도와 방식으로 이용되고 있으며, 개인정보보호를 위해 어떠한 조치가 취해지고 있는지를 알려드립니다.</p>
                <p>또한 개인정보취급방침을 개정하는 경우 웹사이트 공지사항(또는 개별공지)을 통하여 공지할 것입니다.</p>
                <p>개통관련 수집한 개인정보중 당사에서는 이름,배송주소,휴대번호,가입상품등을 제외하고는 개통과 동시에 삭제되며 해당 수집정보는 아래의 목적으로 보관됩니다.</p>
                <ul>
                    <li>사용기간동안의 업무지원 및 민원처리등</li>
                    <li>약정기간사용이후 기기변경에 대한 유선 및 무선 안내</li>
                    <li>해당상품과 관련된 추가상품 마케팅 (보존기간은 해당상품 개통부터 해지후 3개월까지입니다.)</li>
                </ul>
                <hr>
                <label>
                    <input name="agree" type="checkbox" required="required" value="1" @if (old('agree') == '1') checked="checked" @endif> 위의 '개인정보 수집 및 이용' 에 동의합니다.
                </label>
            </div>
            <br>
            <button class="btn btn-primary pull-right" type="submit">신청하기</button>
        </form>
    </div>
@endsection

@section('script')
    <script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
    <script type="text/javascript" src="/js/components/moment/min/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="/js/components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="/js/application.js"></script>
@endsection