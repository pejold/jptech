@extends('layout')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">비밀번호 수정</div>

                <div class="panel-body">
                    <form method="POST" action="{{ route('changePassword') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password_field" class="control-label">새 비밀번호</label>
                            <input id="password_field" type="password" class="form-control" name="password" required>
                            @if ($errors->has('password'))
                            <span class="help-block">{{ $errors->first('password') }}</span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="password-confirm_field" class="control-label">새 비밀번호 확인</label>
                            <input id="password-confirm_field" type="password" class="form-control" name="password_confirmation" required>
                        </div>

                        <button type="submit" class="btn btn-primary btn-block">수정</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
