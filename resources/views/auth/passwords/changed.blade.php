@extends('layout')

@section('content')
    <div class="container standalone">
        <div class="page-header">
            <h2>비밀번호 수정<small></small></h2>
        </div>
        <div class="alert alert-success">
            비밀번호가 성공적으로 변경되었습니다.
        </div>
    </div>
@endsection