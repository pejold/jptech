@extends('layout')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">관리자 로그인</div>

                <div class="panel-body">
                    <form method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('uid') ? ' has-error' : '' }}">
                            <label for="uid_field" class="control-label">아이디</label>
                            <input id="uid_field" type="text" class="form-control" name="uid" value="{{ old('uid') }}" required autofocus>
                            @if ($errors->has('uid'))
                            <span class="help-block">{{ $errors->first('uid') }}</span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password_field" class="control-label">비밀번호</label>
                            <input id="password_field" type="password" class="form-control" name="password" required>
                            @if ($errors->has('password'))
                            <span class="help-block">{{ $errors->first('password') }}</span>
                            @endif
                        </div>

                        {{-- <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div> --}}

                        <button type="submit" class="btn btn-primary btn-block">로그인</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
