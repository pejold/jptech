@extends('layout')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">새 관리자 등록</div>

                <div class="panel-body">
                    <form method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name_field" class="control-label">이름</label>
                            <input id="name_field" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                            @if ($errors->has('name'))
                            <span class="help-block">{{ $errors->first('name') }}</span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('uid') ? ' has-error' : '' }}">
                            <label for="uid_field" class="control-label">아이디</label>
                            <input id="uid_field" type="text" class="form-control" name="uid" value="{{ old('uid') }}" required>
                            @if ($errors->has('uid'))
                            <span class="help-block">{{ $errors->first('uid') }}</span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password_field" class="control-label">비밀번호</label>
                            <input id="password_field" type="password" class="form-control" name="password" required>
                            @if ($errors->has('password'))
                            <span class="help-block">{{ $errors->first('password') }}</span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="password-confirm_field" class="control-label">비밀번호 확인</label>
                            <input id="password-confirm_field" type="password" class="form-control" name="password_confirmation" required>
                        </div>

                        <button type="submit" class="btn btn-primary btn-block">등록</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
