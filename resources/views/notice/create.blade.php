@extends('layout')

@section('title', ':: 공지사항 작성')

@section('content')
    <div class="container standalone">
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/notice">공지사항</a></li>
            <li class="active">작성</li>
        </ol>
        <div class="page-header">
            <h2>공지사항 작성<small></small></h2>
        </div>
        <form enctype="application/x-www-form-urlencoded" action="/notice" method="POST">
            {{ csrf_field() }}
            <div class="form-group @if ($errors->has('title')) has-error @endif">
                <label for="title_field" class="required">제목</label>
                <input id="title_field" name="title" value="{{ old('title') }}" type="text" class="form-control" required="required" />
                <div class="help-block">{{ $errors->first('title') }}</div>
            </div>
            <div class="form-group @if ($errors->has('content')) has-error @endif">
                <label for="question_field" class="required">내용</label>
                <textarea id="question_field" name="content" rows="10" class="form-control" required="required">{{ old('content') }}</textarea>
                <div class="help-block">{{ $errors->first('content') }}</div>
            </div>
            <button class="btn btn-primary pull-right">공지 등록</button>
        </form>
    </div>
@endsection