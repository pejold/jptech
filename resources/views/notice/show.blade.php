@extends('layout')

@section('title', ':: 공지사항')

@section('content')
    <div class="container standalone">
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/notice">공지사항</a></li>
            <li class="active">{{$notice->title}}</li>
        </ol>
        <small class="pull-right"><i class="glyphicon glyphicon-eye-open"></i> {{$notice->view}} 회 | {{$notice->created_at}}</small>
        <div class="page-header">
            <h2>{{$notice->title}} <small></small></h2>
        </div>
        <div>{!! nl2br($notice->content) !!}</div>
        <hr>
        <div class="pull-right">
            @auth
            <a href="/notice/{{$notice->id}}/edit" class="btn btn-default">공지 수정</a>
            @endauth
            <a href="/notice" class="btn btn-default">목록으로</a>
        </div>
        @auth
        <form action="/notice/{{$notice->id}}" method="POST" enctype="application/x-www-form-urlencoded" onsubmit="confirmDelete(event)">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <button type="submit" class="btn btn-danger">공지 삭제</button>
        </form>
        @endauth
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        function confirmDelete(event) {
            if (!confirm('삭제된 공지는 복원할 수 없습니다\n정말 삭제하시겠습니까?')) {
                event.preventDefault();
                event.stopPropagation();
                return false;
            }
        }
    </script>
@endsection