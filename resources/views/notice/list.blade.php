@extends('layout')

@section('title', ':: 공지사항')

@section('content')
    <div class="container standalone">
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li class="active">공지사항</li>
        </ol>
        <div class="page-header">
            <h2>공지사항 <small>:: 총 {{ $notices->total() }} 건</small></h2>
        </div>
        <form action="/notice" method="GET">
            <div class="pull-right search-bar input-group input-group-sm">
                <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                <input name="keyword" type="text" value="{{request('keyword')}}" placeholder="제목 / 내용" class="form-control">
                <span class="input-group-btn">
                    <button class="btn btn-primary" type="submit">검색</button>
                </span>
            </div>
        </form>

        <table class="table">
            <thead>
                <tr>
                    <th class="text-center">순번</th>
                    <th class="text-center">제목</th>
                    <th class="text-center">조회수</th>
                    <th class="text-center">작성일</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($notices as $notice)
                <tr>
                    <td class="text-center">{{$notice->id}}</td>
                    <td class="text-center"><a href="/notice/{{$notice->id}}">{{$notice->title}}</a></td>
                    <td class="text-center">{{$notice->view}}</td>
                    <td class="text-center">{{$notice->created_at}}</td>
                </tr>
                @empty
                <tr>
                    <td colspan="5" class="text-center placeholder">등록된 공지사항이 없습니다.</td>
                </tr>
                @endforelse
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="7" class="text-center">
                        @auth
                        <a href="/notice/create" class="btn btn-default pull-right">공지 작성</a>
                        @endauth
                        {{ $notices->appends(['keyword'=>request('keyword')])->links() }}
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
@endsection