@extends('layout')

@section('title', ':: 문의사항 수정')

@section('content')
    <div class="container standalone">
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/consult">문의사항</a></li>
            <li><a href="/consult/{{$consult->id}}">{{$consult->id}}</a></li>
            <li class="active">문의 수정</li>
        </ol>
        <div class="page-header">
            <h2>문의사항 수정<small></small></h2>
        </div>
        <form enctype="application/x-www-form-urlencoded" action="/consult/{{$consult->id}}" method="POST">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <div class="row">
                <div class="form-group col-sm-4 @if ($errors->has('author')) has-error @endif">
                    <label for="author_field" class="required">작성자</label>
                    <input id="author_field" type="text" name="author" value="{{ old('author')?:$consult->author }}" class="form-control" required="required" />
                    <div class="help-block">{{ $errors->first('author') }}</div>
                </div>
                <div class="form-group col-sm-4 @if ($errors->has('contact')) has-error @endif">
                    <label for="contact_field" class="required">연락처</label>
                    <input id="contact_field" type="tel" name="contact" value="{{ old('contact')?:$consult->contact }}" class="form-control" required="required" />
                    <div class="help-block">{{ $errors->first('contact') }}</div>
                </div>
                <div class="form-group col-sm-4 @if ($errors->has('group')) has-error @endif">
                    <label for="group_field">단체명</label>
                    <input id="group_field" name="group" value="{{ old('group')?:$consult->group }}" type="text" placeholder="단체 문의시 적어주세요" class="form-control" />
                    <div class="help-block">{{ $errors->first('group') }}</div>
                </div>
            </div>
            <div class="form-group @if ($errors->has('title')) has-error @endif">
                <label for="title_field" class="required">제목</label>
                <input id="title_field" name="title" value="{{ old('title')?:$consult->title }}" type="text" class="form-control" required="required" />
                <div class="help-block">{{ $errors->first('title') }}</div>
            </div>
            <div class="form-group @if ($errors->has('question')) has-error @endif">
                <label for="question_field" class="required">내용</label>
                <textarea id="question_field" name="question" rows="10" class="form-control" required="required">{{ old('question')?:$consult->question }}</textarea>
                <div class="help-block">{{ $errors->first('question') }}</div>
            </div>
            <div class="form-group @if ($errors->has('password')) has-error @endif">
                <div class="pull-right">
                    <label><input name="secret" value="1" type="checkbox" @if (old('secret')?:$consult->secret) checked="checked" @endif /> 비밀글</label>
                </div>
                <label for="password_field" class="required">비밀번호</label>
                <input id="password_field" name="password" type="password" placeholder="등록시 입력한 비밀번호를 입력해 주세요" class="form-control" />
                <div class="help-block">{{ $errors->first('password') }}</div>
            </div>
            <button class="btn btn-primary pull-right">문의 수정</button>
        </form>
    </div>
@endsection