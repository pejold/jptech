@extends('layout')

@section('title', ':: 문의사항')

@section('content')
    <div class="container standalone">
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/consult">문의사항</a></li>
            <li class="active">{{$consult->id}}</li>
        </ol>
        <small class="pull-right">@if ($consult->secret) <i class="glyphicon glyphicon-lock"></i>비밀글 | @endif {{$consult->created_at}}</small>
        <div class="page-header">
            <h2>{{$consult->title}} <small>| 작성자 : <strong>{{$consult->author}}</strong> ({{$consult->contact}})</small></h2>
        </div>
        <div>{!! nl2br($consult->question) !!}</div>
        
        @auth
        <div class="well answer">
            <form action="/consult/{{$consult->id}}/answer" method="POST" enctype="application/x-www-form-urlencoded">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="form-group">
                    <label for="answer_field">답변</label>
                    <textarea id="answer_field" name="answer" rows="10" class="form-control" required="required">{{ $consult->answer }}</textarea>
                </div>
                <button class="btn btn-primary pull-right">답변 등록 / 수정</button>
                <div class="clearfix"></div>
            </form>
        </div>
        @else
        @if ($consult->answer)
        <div class="well answer">{!! nl2br($consult->answer) !!}</div>
        @else
        <hr>
        @endif
        @endauth
        <div class="pull-right">
            @if (is_null($consult->answer))
            <a href="/consult/{{$consult->id}}/edit" class="btn btn-default">문의 수정</a>
            @endif
            <a href="/consult" class="btn btn-default">목록으로</a>
        </div>
        <form action="/consult/{{$consult->id}}" method="POST" enctype="application/x-www-form-urlencoded">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <button type="submit" class="btn btn-danger">문의 삭제</button>
        </form>
    </div>
@endsection