@extends('layout')

@section('title', ':: 문의사항 작성')

@section('content')
    <div class="container standalone">
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/consult">문의사항</a></li>
            <li class="active">문의 작성</li>
        </ol>
        <div class="page-header">
            <h2>문의사항 작성<small></small></h2>
        </div>
        <div class="alert alert-success">
            문의사항이 성공적으로 등록되었습니다.
        </div>
    </div>
@endsection