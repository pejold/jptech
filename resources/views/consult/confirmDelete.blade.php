@extends('layout')

@section('title', ':: 문의사항')

@section('content')
    <div class="container standalone">
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/consult">문의사항</a></li>
            <li><a href="/consult/{{$consult->id}}">{{$consult->id}}</a></li>
            <li class="active">삭제</li>
        </ol>
        <small class="pull-right">@if ($consult->secret) <i class="glyphicon glyphicon-lock"></i>비밀글 | @endif {{$consult->created_at}}</small>
        <div class="page-header">
            <h2>{{$consult->title}} <small></small></h2>
        </div>
        <div class="alert alert-warning">
            <p>삭제된 글은 복구 할 수 없습니다!!</p>
            <p>정말 삭제하시려면 작성시 입력한 비밀번호를 확인 해 주세요.</p>
            <br>
            <form action="/consult/{{$consult->id}}" method="POST" enctype="application/x-www-form-urlencoded">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
                <div class="input-group @isset ($error) has-error @endisset">
                    <input id="password_field" name="password" type="password" placeholder="비밀번호" class="form-control" required="required" />
                    <span class="input-group-btn"><button type="submit" class="btn  @isset ($error) btn-danger @else btn-default @endisset">확인</button></span>
                </div>
                @isset ($error)
                <div class="has-error">
                    <span class="help-block">{{ $error }}</span>
                </div>
                @endisset
            </form>
        </div>
    </div>
@endsection