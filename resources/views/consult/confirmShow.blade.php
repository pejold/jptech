@extends('layout')

@section('title', ':: 문의사항')

@section('content')
    <div class="container standalone">
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/consult">문의사항</a></li>
            <li class="active">{{$consult->id}}</li>
        </ol>
        <small class="pull-right">@if ($consult->secret) <i class="glyphicon glyphicon-lock"></i>비밀글 | @endif {{$consult->created_at}}</small>
        <div class="page-header">
            <h2>{{$consult->title}} <small></small></h2>
        </div>
        <div class="alert alert-warning">
            <p>이 게시물은 작성자와 관리자만 열람할 수 있는 비밀글 입니다. 작성시 입력한 비밀번호를 확인 해 주세요.</p>
            <br>
            <form action="/consult/{{$consult->id}}" method="POST" enctype="application/x-www-form-urlencoded">
                {{ csrf_field() }}
                <div class="input-group @if ($errors->has('password')) has-error @endif">
                    <input id="password_field" name="password" type="password" placeholder="비밀번호" class="form-control" required="required" />
                    <span class="input-group-btn"><button type="submit" class="btn  @if ($errors->has('password')) btn-danger @else btn-default @endif">확인</button></span>
                </div>
                <div class="has-error">
                    <span class="help-block">{{ $errors->first('password') }}</span>
                </div>
            </form>
        </div>
    </div>
@endsection