@extends('layout')

@section('title', ':: 문의사항 작성')

@section('style')
    <link rel="stylesheet" href="/js/components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
    <style type="text/css">
        .datetime-help-block {
            margin-top: 36px;
        }
    </style>
@endsection

@section('content')
    <div class="container standalone">
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/consult">문의사항</a></li>
            <li class="active">문의 작성</li>
        </ol>
        <div class="page-header">
            <h2>문의사항 작성<small></small></h2>
        </div>
        <form enctype="application/x-www-form-urlencoded" action="/consult" method="POST">
            {{ csrf_field() }}
            @auth
            <div class="row">
                <div class="form-group col-sm-4 @if ($errors->has('created_at')) has-error @endif">
                    <label for="datetime_field">작성일시</label>
                    <input id="datetime_field" placeholder="년도-월-일 시:분" type='text' name="created_at" value="{{ old('created_at') }}" class="form-control" />
                    <div class="help-block"><small>{{ $errors->first('created_at') }}</small></div>
                </div>
                <div class="col-sm-8 hidden-xs datetime-help-block"><small>작성하지 않으면 현재시간으로 입력됩니다.</small></div>
            </div>
            @endauth
            <div class="row">
                <div class="form-group col-sm-4 @if ($errors->has('author')) has-error @endif">
                    <label for="author_field" class="required">작성자</label>
                    <input id="author_field" type="text" name="author" value="{{ old('author') }}" class="form-control" required="required" />
                    <div class="help-block"><small>{{ $errors->first('author') }}</small></div>
                </div>
                <div class="form-group col-sm-4 @if ($errors->has('contact')) has-error @endif">
                    <label for="contact_field" class="required">연락처</label>
                    <input id="contact_field" type="tel" name="contact" value="{{ old('contact') }}" class="form-control" required="required" />
                    <div class="help-block"><small>{{ $errors->first('contact') }}</small></div>
                </div>
                <div class="form-group col-sm-4 @if ($errors->has('group')) has-error @endif">
                    <label for="group_field">단체명</label>
                    <input id="group_field" name="group" value="{{ old('group') }}" type="text" placeholder="단체 문의시 적어주세요" class="form-control" />
                    <div class="help-block"><small>{{ $errors->first('group') }}</small></div>
                </div>
            </div>
            <div class="form-group @if ($errors->has('title')) has-error @endif">
                <label for="title_field" class="required">제목</label>
                <input id="title_field" name="title" value="{{ old('title') }}" type="text" class="form-control" required="required" />
                <div class="help-block"><small>{{ $errors->first('title') }}</small></div>
            </div>
            <div class="form-group @if ($errors->has('question')) has-error @endif">
                <label for="question_field" class="required">내용</label>
                <textarea id="question_field" name="question" rows="10" class="form-control" required="required">{{ old('question') }}</textarea>
                <div class="help-block"><small>{{ $errors->first('question') }}</small></div>
            </div>
            <div class="form-group @if ($errors->has('password')) has-error @endif">
                <div class="pull-right">
                    <label><input name="secret" value="1" type="checkbox" @if (old('secret')) checked="checked" @endif /> 비밀글</label>
                </div>
                <label for="password_field" class="required">비밀번호</label>
                <input id="password_field" name="password" type="password" placeholder="수정 / 삭제 / 비밀글 조회시 사용됩니다" class="form-control" required="required" />
                <div class="help-block"><small>{{ $errors->first('password') }}</small></div>
            </div>
            <button class="btn btn-primary pull-right">문의 등록</button>
        </form>
    </div>
@endsection

@section('script')
    <script type="text/javascript" src="/js/components/moment/min/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="/js/components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#datetime_field').datetimepicker({
                locale: 'ko',
                format: 'YYYY-MM-DD HH:mm'
            });
        });
    </script>
@endsection