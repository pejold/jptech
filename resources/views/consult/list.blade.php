@extends('layout')

@section('title', ':: 문의사항')

@section('content')
    <div class="container standalone">
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li class="active">문의사항</li>
        </ol>
        <div class="page-header">
            <h2>문의사항 <small>:: 총 {{ $consults->total() }} 건</small></h2>
        </div>
        <form action="/consult" method="GET">
            <div class="pull-right search-bar input-group input-group-sm">
                <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                <input name="keyword" type="text" value="{{request('keyword')}}" placeholder="작성자 / 단체명 / 제목" class="form-control">
                <span class="input-group-btn">
                    <button class="btn btn-primary" type="submit">검색</button>
                </span>
            </div>
        </form>

        <table class="table">
            <thead>
                <tr>
                    <th class="text-center">순번</th>
                    <th class="text-center">답변</th>
                    <th class="text-center">제목</th>
                    <th class="text-center">단체명</th>
                    <th class="text-center">작성자</th>
                    <th class="text-center">작성일</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($consults as $consult)
                <tr>
                    <td class="text-center">{{$consult->id}}</td>
                    <td class="text-center">
                        @if ($consult->answer)
                        <i class="glyphicon glyphicon-check"></i>
                        @endif
                    </td>
                    <td class="text-center"><a href="/consult/{{$consult->id}}">@if ($consult->secret) <i class="glyphicon glyphicon-lock"></i> @endif {{$consult->title}}</a></td>
                    <td class="text-center">{{$consult->group}}</td>
                    <td class="text-center">{{$consult->author}}</td>
                    <td class="text-center">{{$consult->created_at}}</td>
                </tr>
                @empty
                <tr>
                    <td colspan="5" class="text-center placeholder">등록된 문의사항이 없습니다.</td>
                </tr>
                @endforelse
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="7" class="text-center">
                        <a href="/consult/create" class="btn btn-default pull-right">문의 작성</a>
                        {{ $consults->appends(['keyword'=>request('keyword')])->links() }}
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
@endsection