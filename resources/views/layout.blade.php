<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>제이피테크 @yield('title', '')</title>

    <link rel="stylesheet" type="text/css" href="/css/app.css?<?=(new DateTime())->getTimestamp();?>">
    @yield('style')
</head>
<body>
    <header>
        @auth
        <nav class="admin-navbar">
            <strong>관리자 모드</strong>
            <ul class="pull-right">
                <li><a href="/carousel">캐러셀 이미지 관리</a></li>
                <li><a href="/banner/edit">메인화면 배너 관리</a></li>
                <li><a href="/notice/create">공지사항 작성</a></li>
                {{-- <li><a href="/admin/register">관리자 등록</a></li> --}}
                <li><a href="/admin/edit">비밀번호 수정</a></li>
                <li><a href="/admin/logout">로그아웃</a></li>
            </ul>
        </nav>
        @endauth
        <nav class="navbar navbar-default">
            <div class="container">
                <span class="navbar-right contact-number hidden-xs text-info"><i class="glyphicon glyphicon-phone-alt"></i> 1588-0000</span>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main_menu" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="/" class="navbar-brand"><img src="/images/LGUplus.png" alt="LG Uplus"></a>
                </div>
                <div class="collapse navbar-collapse" id="main_menu">
                    <ul class="nav navbar-nav">
                        @foreach ($products as $product)
                        <li><a href="/product/{{ $product->name }}">{{ $product->name }}</a></li>
                        @endforeach
                        <li><a href="/application/create">가입신청</a></li>
                        <li><a href="/application">신청현황</a></li>
                        <li><a href="/consult">문의사항</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <main>@yield('content')</main>
    <footer>
        <div class="container">
            <nav class="pull-right">
                <ul class="small">
                    <li><a href="#" data-toggle="modal" data-target="#termsModal">이용약관</a></li>
                    <li><a href="#" data-toggle="modal" data-target="#privacyModal">개인정보취급방침</a></li>
                </ul>
            </nav>
            <div class="media">
                <div class="media-left">
                    <img src="/images/LGUplus.png" alt="LG Uplus">
                </div>
                <div class="media-body">
                    <small><strong>대표</strong> 손 제 필 | <strong>사업자등록번호</strong> 432-86-00685 | <strong>통신판매업신고번호</strong> 제 호</small>
                    <address class="small"><strong>주소</strong> 부산시 해운대구 센텀중앙로 97 센텀스카이비즈 A동 1904호</address>
                    <small class="copyright">Copyright &copy; <strong>JP Tech</strong> All Rights Reserved.</small>
                </div>
            </div>
        </div>
    </footer>
    <div class="modal fade" id="termsModal" tabindex="-1" role="dialog" aria-labelledby="termsModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="termsModalLabel">이용약관</h4>
                </div>
                <div class="modal-body">
                    <p>...</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="privacyModal" tabindex="-1" role="dialog" aria-labelledby="privacyModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="privacyModalLabel">개인정보 취급방침</h4>
                </div>
                <div class="modal-body">
                    <p>...</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="/js/app.js?<?=(new DateTime())->getTimestamp();?>"></script>
    @yield('script')
</body>
</html>
