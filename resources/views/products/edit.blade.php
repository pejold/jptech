@extends('layout')

@section('title', ":: 상품수정")

@section('style')
    <link href="/js/components/summernote/dist/summernote.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/product.css">
@endsection

@section('content')
    <div class="container standalone">
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/product/{{ $product->name }}">{{ $product->name }}</a></li>
            <li class="active">수정</li>
        </ol>
        <form action="/product/{{ $product->name }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <div class="form-group {{ ($errors->has('name')) ? 'has-error' : '' }}">
                <label for="name_field" class="control-label required">상품명</label>
                <input id="name_field" name="name" class="form-control" value="{{ old('name')?:$product->name }}" required="required">
                <span class="help-block"><small>{{$errors->first('name')}}</small></span>
            </div>
            <div class="form-group {{ ($errors->has('sub_title')) ? 'has-error' : '' }}">
                <label for="sub_title_field" class="control-label">부제목</label>
                <input id="sub_title_field" name="sub_title" class="form-control" value="{{ old('sub_title')?:$product->sub_title }}">
                <span class="help-block"><small>{{$errors->first('sub_title')}}</small></span>
            </div>
            {{-- <div class="checkbox {{ ($errors->has('visible')) ? 'has-error' : '' }}">
                <label for="visible_field" class="control-label">
                    <input type="checkbox" id="visible_field" name="visible" value="1" {{ (old('visible') == '1') || (!old('visible') && $product->visible == '1') ? 'checked="checked"' : '' }}>
                    상품 노출
                </label>
            </div>  --}}
            <input type="hidden" name="visible" value="1">
            <hr>
            <div class="row">
                <div class="col-sm-6 col-md-8">
                    <div class="form-group {{ ($errors->has('banner_image')) ? 'has-error' : '' }}">
                        <label for="banner_image_field" class="control-label">배너이미지 <small>( 가로 475px / 세로 230px )</small></label>
                        <input id="banner_image_field" name="banner_image" type="file" class="form-control" value="{{ old('banner_image') }}">
                        <span class="help-block"><small>{{$errors->first('banner_image')}}</small></span>
                    </div>
                    <div class="checkbox {{ ($errors->has('image_only')) ? 'has-error' : '' }}">
                        <label for="image_only_field" class="control-label">
                            <input type="checkbox" id="image_only_field" name="image_only" value="1" {{ (old('image_only') == '1') || (!old('image_only') && $product->image_only == '1') ? 'checked="checked"' : '' }}>
                            메인화면에 배너이미지만 노출 (배너이미지 위에 상품명 / 부제목 / 설명 오버레이 없앰)
                        </label>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <img src="{{ $product->banner_url }}" alt="배너이미지 미리보기" id="preview" class="thumbnail">
                </div>
            </div>
            
            <hr>
            <div class="form-group {{ ($errors->has('short_description')) ? 'has-error' : '' }}">
                <label for="short_description_field" class="control-label">설명</label>
                <input id="short_description_field" name="short_description" class="form-control" value="{{ old('short_description')?:$product->short_description }}">
                <span class="help-block"><small>{{$errors->first('short_description')}}</small></span>
            </div>
            <div class="form-group {{ ($errors->has('description')) ? 'has-error' : '' }}">
                <label for="description_field" class="control-label required">상세 설명</label>
                <textarea id="description_field" name="description" class="form-control">{{ old('description')?:$product->description }}</textarea>
                <span class="help-block"><small>{{$errors->first('description')}}</small></span>
            </div>
            <button type="submit" class="btn btn-primary">수정</button>
        </form>
    </div>
@endsection

@section('script')
    <script src="/js/components/summernote/dist/summernote.min.js"></script>
    <script src="/js/components/summernote/dist/lang/summernote-ko-KR.min.js"></script>
    <script type="text/javascript" src="/js/product.js"></script>
@endsection