@extends('layout')

@section('title', ":: $product->name")

@section('content')
    <div class="container standalone">
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li class="active">{{ $product->name }}</li>
        </ol>
        <div class="page-header"> 
            @auth
            <a href="/product/{{ $product->name }}/edit" class="btn btn-default pull-right">수정</a>
            @endauth
            <h2>{{ $product->name }} <small>{{ $product->sub_title }}</small></h2> 
        </div> 
        <div class="content">{!! $product->description !!}</div>
    </div>
@endsection