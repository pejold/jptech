@extends('layout')

@section('style')
    <style type="text/css">
        .alert.price-banner-wide {
            padding: 0;
            overflow: hidden;
            height: 100px;
        }
        .price-panel {
            overflow: hidden;
        }
        .fillwidth {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="jumbotron">
        <div class="container">
            <div id="carousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                    @foreach ($images as $image)
                    <div class="item{{ ($image->id == 1)?' active':'' }}">
                        <img src="{{ $image->image_url }}" alt="">
                    </div>
                    @endforeach
                </div>
                <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            @foreach ($products as $product)
            <div class="middle-box col-lg-4 col-sm-6 col-xs-12">
                <a href="/product/{{ $product->name }}">
                    @if (!$product->image_only)
                    <h3>{{ $product->name }} <small>{{ $product->sub_title }}</small></h3>
                    <div class="description">{{ $product->short_description }}</div>
                    @endif
                    <img src="{{ $product->banner_url }}" alt="LFM-100" class="img-responsive img-thumbnail">
                </a>
            </div>
            @endforeach
            <div class="middle-box col-sm-6 hidden-lg">
                <img src="/storage/banners/sale_narrow.png" alt="파격가" class="img-responsive img-thumbnail">
            </div>
            <div class="col-lg-12 visible-lg-block">
                <div class="alert alert-danger price-banner-wide">
                    <img src="/storage/banners/sale_wide.png" alt="파격가">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 col-md-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <a href="/consult/create" class="close"><span aria-hidden="true">+</span></a>
                        <h3 class="panel-title">빠른 문의작성</h3>
                    </div>
                    <div class="panel-body">
                        <form action="/consult" method="POST" enctype="application/x-www-form-urlencoded">
                            {{ csrf_field() }}
                            <input type="hidden" name="title" value="빠른문의" />
                            <input type="hidden" name="password" value="{{ str_random(20) }}" />
                            <div class="form-group">
                                <label class="small" for="author_field">작성자</label>
                                <input type="text" id="author_field" name="author" class="form-control input-sm" required="required">
                            </div>
                            <div class="form-group">
                                <label class="small" for="contact_field">연락처</label>
                                <input type="tel" id="contact_field" name="contact" class="form-control input-sm" required="required">
                            </div>
                            <div class="form-group">
                                <textarea name="question" class="form-control input-sm" placeholder="이곳에 문의사항을 남겨주시면 성심성의껏 상담해 드리도록 하겠습니다." required="required"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary btn-block">문의하기</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-8 col-md-6">
                <div class="panel panel-primary request-panel">
                    <div class="panel-heading">
                        <a href="/application" class="close" aria-label="더보기"><span aria-hidden="true">+</span></a>
                        <h3 class="panel-title">신청현황</h3>
                    </div>
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="small text-center">고객명</th>
                                <th class="small text-center">핸드폰 뒷자리</th>
                                <th class="small text-center">제품명</th>
                                <th class="small text-center">접수일자</th>
                                <th class="small text-center">상태</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($applications as $application)
                            <tr>
                                <td class="text-center small text-primary">{{$application->name}}</td>
                                <td class="text-center small">{{substr($application->mobile, -4)}}</td>
                                <td class="text-center small">{{$application->product}}</td>
                                <td class="text-center small">{{$application->created_at->toDateString()}}</td>
                                <td class="text-center">
                                @if ($application->status == 'waiting')
                                    <span class="label label-default">접수대기</span>
                                @elseif ($application->status == 'accepted')
                                    <span class="label label-default">접수완료</span>
                                @elseif ($application->status == 'ready_for_ship')
                                    <span class="label label-primary">발송준비</span>
                                @elseif ($application->status == 'sent')
                                    <span class="label label-danger">발송완료</span>
                                @endif
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="5" class="text-center placeholder">등록된 신청내역이 없습니다.</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-sm-12 col-md-3">
                <div class="alert alert-info">
                    <h4>지금바로 전화주세요!</h4>
                    <div class="text-center text-xxlarge"><strong>1588-0000</strong></div>
                    <button class="btn btn-danger btn-block btn-middle">
                        <i class="glyphicon glyphicon-earphone"></i> 상담신청하기
                    </button>
                    <p class="text-center text-muted"><i class="glyphicon glyphicon-time"></i> 평일 09시~18시 까지</p>
                    <p><span class="text-danger">주말은 발송이 불가</span>하여 평일에 최우선으로 처리해 드리겠습니다.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="panel panel-default board-panel">
                    <div class="panel-heading">
                        <a href="/consult" class="close" aria-label="더보기"><span aria-hidden="true">+</span></a>
                        <h3 class="panel-title">문의사항</h3>
                    </div>
                    <table class="table">
                        @forelse ($consults as $consult)
                        <tr>
                            <td>@if ($consult->secret) <i class="glyphicon glyphicon-lock"></i> @endif <a href="/consult/{{ $consult->id }}">{{ $consult->title }}</a></td>
                            <td class="text-right small">{{ $consult->created_at->toDateString() }}</td>
                        </tr>
                        @empty
                        <tr>
                            <td class="text-center placeholder">등록된 문의사항이 없습니다.</td>
                        </tr>
                        @endforelse
                    </table>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="panel panel-default board-panel">
                    <div class="panel-heading">
                        <a href="/notice" class="close" aria-label="더보기"><span aria-hidden="true">+</span></a>
                        <h3 class="panel-title">공지사항</h3>
                    </div>
                    <table class="table">
                        @forelse ($notices as $notice)
                        <tr>
                            <td><a href="/notice/{{$notice->id}}">{{$notice->title}}</a></td>
                            <td class="text-right small">{{$notice->created_at->toDateString()}}</td>
                        </tr>
                        @empty
                        <tr>
                            <td class="text-center placeholder">등록된 공지사항이 없습니다.</td>
                        </tr>
                        @endforelse
                    </table>
                </div>
            </div>
        </div>
        <div class="panel panel-default row price-panel">
            <img src="/storage/banners/price.png" class="fillwidth" alt="요금안내">
            {{-- <div class="panel-body">
                <p>전국 어디서나 들고다니며 사용 가능한 휴대용 Wifi</p>
                <h3><span class="text-danger">U+</span> 모바일라우터 요금안내</h3>
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="panel panel-danger">
                            <div class="panel-heading">LTE 10G</div>
                            <table class="table">
                                <tr>
                                    <td>기본데이터</td>
                                    <td class="text-right">10 GB</td>
                                </tr>
                                <tr>
                                    <td>기본료</td>
                                    <td class="text-right">15,000 원</td>
                                </tr>
                                <tr>
                                    <td>가입비</td>
                                    <td class="text-right">0 원</td>
                                </tr>
                                <tr>
                                    <td>유심비</td>
                                    <td class="text-right">0 원</td>
                                </tr>
                                <tr>
                                    <td>단말기 할부원금</td>
                                    <td class="text-right">0 원</td>
                                </tr>
                            </table>
                            <div class="panel-footer text-right">월 청구요금 15,000 원</div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="panel panel-info">
                            <div class="panel-heading">LTE 20G</div>
                            <table class="table">
                                <tr>
                                    <td>기본데이터</td>
                                    <td class="text-right">20 GB</td>
                                </tr>
                                <tr>
                                    <td>기본료</td>
                                    <td class="text-right">22,500 원</td>
                                </tr>
                                <tr>
                                    <td>가입비</td>
                                    <td class="text-right">0 원</td>
                                </tr>
                                <tr>
                                    <td>유심비</td>
                                    <td class="text-right">0 원</td>
                                </tr>
                                <tr>
                                    <td>단말기 할부원금</td>
                                    <td class="text-right">0 원</td>
                                </tr>
                            </table>
                            <div class="panel-footer text-right">월 청구요금 22,500 원</div>
                        </div>
                    </div>
                </div>
                <ul>
                    <li>의무약정 24개월 / VAT 별도</li>
                    <li>약정기간 만료 전 이용계약 해지(요금미납, 단말기분실,파손 등 사유 포함), 명의변경(양수인이 의무약정 승계 않는 경우), 약정 철회시에는 위약금이 청구됩니다.</li>
                    <li>위약금 = 약정금액 X ((약정기간 - 사용기간) % 약정기간)</li>
                </ul>
            </div> --}}
        </div>
        {{-- <ol class="steps row hidden-xs">
            <li class="col-xs-4">
                <div class="step">
                    <div class="media">
                        <div class="media-left text-xxlarge"><i class="glyphicon glyphicon-list-alt"></i></div>
                        <div class="media-body">
                            <h4 class="media-heading">신청서 작성</h4>
                            <small class="hidden-xs">원하는 상품 신청서 접수</small>
                        </div>
                    </div>
                </div>
            </li>
            <li class="col-xs-4">
                <div class="step">
                    <div class="media">
                        <div class="media-left text-xxlarge"><i class="glyphicon glyphicon-phone-alt"></i></div>
                        <div class="media-body">
                            <h4 class="media-heading">해피콜 진행</h4>
                            <small class="hidden-xs">고객님께 해피콜 진행</small>
                        </div>
                    </div>
                </div>
            </li>
            <li class="col-xs-4">
                <div class="step">
                    <div class="media">
                        <div class="media-left text-xxlarge"><i class="glyphicon glyphicon-gift"></i></div>
                        <div class="media-body">
                            <h4 class="media-heading">상품 발송</h4>
                            <small class="hidden-xs">상품 개통 후 고객님께 발송</small>
                        </div>
                    </div>
                </div>
            </li>
        </ol> --}}
        <div class="row">
            <div class="col-xs-12">
                <img src="/images/process.png" class="fillwidth" alt="진행안내">
            </div>
        </div>
    </div>
@endsection