@extends('layout')

@section('title', ":: 메인화면 이미지 수정")

@section('style')
<style type="text/css">
    .thumbnail {
        width: 100%;
    }
    .thumbnail.narrow {
        min-height: 100px;
    }
</style>
@endsection

@section('content')
    <div class="container standalone">
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li class="active">메인화면 이미지 수정</li>
        </ol>
        <form action="/banner" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <div class="row">
                <div class="col-sm-6 col-md-8">
                    <div class="form-group {{ ($errors->has('sale_narrow_image')) ? 'has-error' : '' }}">
                        <label for="sale_narrow_image_field" class="control-label">파격가배너 이미지 <small>( 가로 475px / 세로 230px )</small></label>
                        <input id="sale_narrow_image_field" name="sale_narrow_image" type="file" class="form-control" value="{{ old('sale_narrow_image') }}">
                        <span class="help-block"><small>{{$errors->first('sale_narrow_image')}}</small></span>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <img src="/storage/banners/sale_narrow.png" alt="파격가배너 이미지 미리보기" id="sale_narrow_image_preview" class="thumbnail narrow">
                </div>
            </div>
            <div class="form-group {{ ($errors->has('sale_wide_image')) ? 'has-error' : '' }}">
                <label for="sale_wide_image_field" class="control-label">파격가배너 이미지 긴 버전<small>( 가로 1158px 이상 )</small></label>
                <input id="sale_wide_image_field" name="sale_wide_image" type="file" class="form-control" value="{{ old('sale_wide_image') }}">
                <span class="help-block"><small>{{$errors->first('sale_wide_image')}}</small></span>
            </div>
            <img src="/storage/banners/sale_wide.png" alt="파격가배너 이미지 긴 버전 미리보기" id="sale_wide_image_preview" class="thumbnail">
            <hr>
            <div class="row">
                <div class="col-sm-6 col-md-8">
                    <div class="form-group {{ ($errors->has('price_image')) ? 'has-error' : '' }}">
                        <label for="price_image_field" class="control-label">요금안내 이미지 <small>( 가로 1158px 이상 )</small></label>
                        <input id="price_image_field" name="price_image" type="file" class="form-control" value="{{ old('price_image') }}">
                        <span class="help-block"><small>{{$errors->first('price_image')}}</small></span>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <img src="/storage/banners/price.png" alt="요금안내이미지 미리보기" id="price_image_preview" class="thumbnail narrow">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">수정</button>
        </form>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(function () {
            $('input[type=file]').change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    var prev = $('#'+this.name+'_preview');
                    reader.onload = function (e) {
                        prev.attr('src', e.target.result);
                    }
                    reader.readAsDataURL(this.files[0]);
                }
            });
        });
    </script>
@endsection