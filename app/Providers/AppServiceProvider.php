<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Product;
use Hash;
use Validator;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $products = Product::where('visible', true)->get();
        View::share('products', $products);

        Validator::extend('check', function ($attribute, $value, $parameters, $validator) {
            return Hash::check($value, $parameters[0]);
        });

        Validator::extend('phone', function($attribute, $value, $parameters, $validator) {
            return $value == '' || preg_match('/^(\d{8,11}|\d{2,3}-\d{3,4}-\d{4})$/', $value);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
