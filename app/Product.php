<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [];
    protected $casts = ['visible' => 'boolean'];

    public function getRouteKeyName()
    {
    	return 'name';
    }
}
