<?php

function format_phone($number){
    $number = preg_replace("/[^0-9]/", "", $number);
    return preg_replace("/(^02.{0}|^01.{1}|[0-9]{3})([0-9]+)([0-9]{4})/", "$1-$2-$3", $number);
}