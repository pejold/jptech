<?php

namespace App\Http\Controllers;

use App\CarouselImage;
use Illuminate\Http\Request;
use DB;
use Storage;

class CarouselImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images = CarouselImage::all();
        return view('carousel.list', compact('images'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('carousel.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'new_image' => 'required|image'
        ]);

        $id = CarouselImage::count() + 1;
        $imageFile = $request->file('new_image');
        $filename = uniqid().'.'.$imageFile->extension();
        $imageFile->storeAs('carousel', $filename, 'public');

        CarouselImage::create([
            'id' => $id,
            'image_url' => '/storage/carousel/'.$filename
        ]);

        return redirect('/carousel');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CarouselImage  $carouselImage
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('carousel.edit', ['image' => CarouselImage::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CarouselImage  $carouselImage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'image' => 'required|image'
        ]);

        $imageFile = $request->file('image');
        $filename = uniqid().'.'.$imageFile->extension();
        $imageFile->storeAs('carousel', $filename, 'public');

        $image = CarouselImage::find($id);
        Storage::disk('public')->delete(str_replace('/storage/', '', $image->image_url));
        $image->image_url = '/storage/carousel/'.$filename;
        $image->save();

        return redirect('/carousel');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CarouselImage  $carouselImage
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $image = CarouselImage::find($id);
        Storage::disk('public')->delete(str_replace('/storage/', '', $image->image_url));
        $image->delete();

        CarouselImage::where('id', '>', $id)->update(['id' => DB::raw('id - 1')]);

        return redirect('/carousel');
    }
}
