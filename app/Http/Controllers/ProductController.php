<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use DOMDocument;
use Image;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|unique:products',
            'sub_title' => 'nullable',
            'short_description' => 'nullable',
            'description' => 'required',
            'visible' => 'nullable|boolean',
            'image_only' => 'nullable|boolean',
            'banner_image' => 'required|image|dimensions:width=475,heigh=230'
        ]);
        $bannerImage = $request->file('banner_image');
        $bannerFileName = $data['name'].'.'.$bannerImage->clientExtension();
        $bannerImage->storeAs('images', $bannerFileName, 'public');

        $data['description'] = $this->saveImages($data['description']);
        $data['banner_url'] = "/storage/images/$bannerFileName";
        unset($data['banner_image']);

        $product = Product::create($data);

        return redirect('/product/'.$product->name);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('products/detail', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('products/edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $data = $request->validate([
            'name' => [
                'required',
                Rule::unique('products')->ignore($product->id)
            ],
            'sub_title' => 'nullable',
            'short_description' => 'nullable',
            'description' => 'required',
            'visible' => 'boolean',
            'image_only' => 'nullable|boolean',
            'banner_image' => 'nullable|image|dimensions:width=475,heigh=230'
        ]);
        $data['description'] = $this->saveImages($data['description']);
        if (!isset($data['visible'])) $data['visible'] = false;
        if (!isset($data['image_only'])) $data['image_only'] = false;
        if ($request->hasFile('banner_image')) {
            $bannerImage = $request->file('banner_image');
            $bannerFileName = $data['name'].'.'.$bannerImage->clientExtension();
            $bannerImage->storeAs('images', $bannerFileName, 'public');
            unset($data['banner_image']);
        }

        $product->update($data);

        return redirect('/product/'.$product->name);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }

    private function saveImages($content)
    {
        $dom = new DOMDocument();
        $dom->loadHtml(mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED|LIBXML_HTML_NODEFDTD);
        $images = $dom->getElementsByTagName('img');
        
        // foreach <img> in the submited message
        foreach ($images as $img) {
            $src = $img->getAttribute('src');

            // if the img source is 'data-url'
            if(preg_match('/data:image/', $src)){

                // get the mimetype
                preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                $mimetype = $groups['mime'];

                // Generating a random filename
                $filename = uniqid();
                $filepath = "/images/products/$filename".'.'.$mimetype;

                // @see http://image.intervention.io/api/
                $image = Image::make($src)
                    // resize if required
                    /* ->resize(300, 200) */
                    ->encode($mimetype, 100)  // encode file to the specified mimetype
                    ->save(public_path($filepath));

                $image = Image::make($src);
                // resize if required
                if ($image->width() > 1110) {
                    $image->resize(1110, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                // encode file to the specified mimetype
                $image->encode($mimetype, 100)->save(public_path($filepath));
                
                // $new_src = asset($filepath);
                $img->removeAttribute('src');
                $img->setAttribute('src', $filepath);
            }
        }
        return $dom->saveHTML();
    }
}
