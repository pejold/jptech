<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BannerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('show');
    }

    public function edit()
    {
        return view('editbanner');
    }

    public function update(Request $request)
    {
        $data = $request->validate([
            'sale_narrow_image' => 'nullable|image|mimes:png|dimensions:width=475,heigh=230',
            'sale_wide_image' => 'nullable|image|mimes:png|dimensions:min-width=1158',
            'price_image' => 'nullable|image|mimes:png|dimensions:min-width=1158'
        ]);

        if ($request->hasFile('sale_narrow_image')) {
            $request->file('sale_narrow_image')->storeAs('banners', 'sale_narrow.png', 'public');
        }
        if ($request->hasFile('sale_wide_image')) {
            $request->file('sale_wide_image')->storeAs('banners', 'sale_wide.png', 'public');
        }
        if ($request->hasFile('price_image')) {
            $request->file('price_image')->storeAs('banners', 'price.png', 'public');
        }

        return redirect('/');
    }
}
