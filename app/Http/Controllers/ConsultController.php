<?php

namespace App\Http\Controllers;

use App\Consult;
use Illuminate\Http\Request;
use Hash;

class ConsultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = Consult::orderBy('created_at', 'DESC');
        if (request()->has('keyword')) {
            $keyword = request('keyword');
            $query->where('author', $keyword);
            $query->orWhere('group', $keyword);
            $query->orWhere('title', 'like', '%'.$keyword.'%');
        }
        $consults = $query->paginate();
        if (auth()->guest()) {
            $consults->map(function ($consult) {
                $length = mb_strlen($consult->author);
                switch ($length) {
                    case 1:
                        $consult->author = '*';
                        break;
                    case 2:
                        $consult->author = mb_substr($consult->author, 0, 1).'*';
                        break;
                    default:
                        $consult->author = mb_substr($consult->author, 0, 1).str_repeat('*', $length - 2).mb_substr($consult->author, -1);
                        break;
                }
            });
        }
        return view('consult/list', compact('consults'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('consult/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'author' => 'required|string',
            'title' => 'required|string',
            'contact' => 'required|phone',
            'question' => 'required|string',
            'password' => 'required',
            'secret' => 'boolean',
            'group' => 'nullable',
            'created_at' => 'nullable|date_format:Y-m-d H:i'
        ]);

        $data['password'] = bcrypt($data['password']);
        $data['contact'] = str_replace('-', '', $data['contact']);
        if (isset($data['created_at'])) $data['created_at'] .= ':00';

        $consult = Consult::create($data);

        return view('consult/created', compact('consult'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Consult  $consult
     * @return \Illuminate\Http\Response
     */
    public function show(Consult $consult)
    {
        if (auth()->guest()) {
            if ($consult->secret) {
                if (request()->isMethod('POST')) {
                    request()->validate(['password'=>'required|check:'.$consult->password]);
                } else {
                    return view('consult/confirmShow', compact('consult'));
                }
            } else {
                $length = mb_strlen($consult->author);
                switch ($length) {
                    case 1:
                        $consult->author = '*';
                        break;
                    case 2:
                        $consult->author = mb_substr($consult->author, 0, 1).'*';
                        break;
                    default:
                        $consult->author = mb_substr($consult->author, 0, 1).str_repeat('*', $length - 2).mb_substr($consult->author, -1);
                        break;
                };
                $consult->contact = mb_substr($consult->contact, 0, 3).str_repeat('*', max(mb_strlen($consult->contact) - 7, 0)).mb_substr($consult->contact, -4);
            }
        }
        return view('consult/show', compact('consult'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Consult  $consult
     * @return \Illuminate\Http\Response
     */
    public function edit(Consult $consult)
    {
        return view('consult/edit', compact('consult'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Consult  $consult
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Consult $consult)
    {
        $data = $request->validate([
            'author' => 'required|string',
            'title' => 'required|string',
            'contact' => 'required|phone',
            'question' => 'required|string',
            'password' => 'required|check:'.$consult->password,
            'secret' => 'boolean',
            'group' => 'nullable'
        ]);

        unset($data['password']);
        $data['contact'] = str_replace('-', '', $data['contact']);

        $consult->update($data);
        return redirect("/consult/$consult->id");
    }

    public function answer(Request $request, Consult $consult)
    {   
        $data = $request->validate([
            'answer' => 'required|string'
        ]);

        $consult->update($data);
        return redirect("/consult/$consult->id");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Consult  $consult
     * @return \Illuminate\Http\Response
     */
    public function destroy(Consult $consult)
    {
        if (auth()->guest()) {
            if (request()->has('password')) {
                if (!Hash::check(request('password'), $consult->password)) {
                    $error = '비밀번호가 올바르지 않습니다.';
                    return view('consult/confirmDelete', compact('consult', 'error'));
                }
            } else {
                return view('consult/confirmDelete', compact('consult'));
            }
        }

        $consult->delete();
        return redirect('/consult');
    }
}
