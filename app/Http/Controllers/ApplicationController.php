<?php

namespace App\Http\Controllers;

use App\Application;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ApplicationController extends Controller
{
    private $statusLabels = [
        'waiting' => '접수대기',
        'accepted' => '접수완료',
        'ready_for_ship' => '발송준비',
        'sent' => '발송완료',
    ];

    private $statusColors = [
        'waiting' => 'default',
        'accepted' => 'default',
        'ready_for_ship' => 'primary',
        'sent' => 'danger',
    ];

    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'create', 'store']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = Application::latest();
        if (request()->has('keyword')) {
            $query->where('mobile', 'like', '%'.request('keyword'));
        }
        $applications = $query->paginate();
        if (auth()->guest()) {
            $applications->map(function ($application) {
                $length = mb_strlen($application->name);
                switch ($length) {
                    case 1:
                        $application->name = '*';
                        break;
                    case 2:
                        $application->name = mb_substr($application->name, 0, 1).'*';
                        break;
                    default:
                        $application->name = mb_substr($application->name, 0, 1).str_repeat('*', $length - 2).mb_substr($application->name, -1);
                        break;
                }
            });
        }

        return view('application/list', [
            'applications' => $applications,
            'statusLabels' => $this->statusLabels,
            'statusColors' => $this->statusColors
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('application/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'product' => ['required', Rule::in(['LFM-100', 'ME-Y30K', 'HS-2300'])],
            'plan' => ['required', Rule::in(['10G', '20G'])],
            'contract' => ['required', Rule::in('2Y')],
            'customer_type' => ['required', Rule::in(['personal', 'business'])],
            'name' => 'required|string',
            'telephone' => 'nullable|phone',
            'mobile' => 'required|phone',
            'postcode' => 'required|numeric',
            'address' => 'required|string',
            'address_detail' => 'required|string',
            'recommander' => 'nullable|string',
            'payment_type' => ['nullable', Rule::in(['card', 'bank'])],
            'card_type' => 'required_if:payment_type,card|nullable|string',
            'card_number' => 'required_if:payment_type,card|nullable|numeric',
            'expire_year' => 'required_if:payment_type,card|nullable|date_format:Y',
            'expire_month' => 'required_if:payment_type,card|nullable|date_format:m',
            'bank_name' => 'required_if:payment_type,bank|nullable|string',
            'bank_account_number' => 'required_if:payment_type,bank|nullable|numeric',
            'memo' => 'nullable|string',
            'agree' => 'accepted',
            'created_at' => 'nullable|date_format:Y-m-d H:i'
        ]);

        $data['telephone'] = str_replace('-', '', $data['telephone']);
        $data['mobile'] = str_replace('-', '', $data['mobile']);
        $data['postcode'] = str_replace('-', '', $data['postcode']);
        $data['card_number'] = str_replace('-', '', $data['card_number']);
        $data['bank_account_number'] = str_replace('-', '', $data['bank_account_number']);
        if (isset($data['payment_type']) && $data['payment_type'] == 'card') {
            $data['expire_date'] = $data['expire_year'].'-'.$data['expire_month'].'-01';
        }
        if (isset($data['created_at'])) $data['created_at'] .= ':00';
        unset($data['agree']);
        unset($data['expire_year']);
        unset($data['expire_month']);

        $application = Application::create($data);

        return view('application/created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function show(Application $application)
    {
        return view('application/show', [
            'application' => $application,
            'statusLabels' => $this->statusLabels,
            'statusColors' => $this->statusColors
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function edit(Application $application)
    {
        return view('application/edit', compact('application'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Application $application)
    {
        $data = $request->validate([
            'product' => ['required', Rule::in(['LFM-100', 'ME-Y30K', 'HS-2300'])],
            'plan' => ['required', Rule::in(['10G', '20G'])],
            'contract' => ['required', Rule::in('2Y')],
            'customer_type' => ['required', Rule::in(['personal', 'business'])],
            'name' => 'required|string',
            'telephone' => 'nullable|phone',
            'mobile' => 'required|phone',
            'postcode' => 'required|numeric',
            'address' => 'required|string',
            'address_detail' => 'required|string',
            'recommander' => 'nullable|string',
            'payment_type' => ['nullable', Rule::in(['card', 'bank'])],
            'card_type' => 'required_if:payment_type,card|nullable|string',
            'card_number' => 'required_if:payment_type,card|nullable|numeric',
            'expire_year' => 'required_if:payment_type,card|nullable|date_format:Y',
            'expire_month' => 'required_if:payment_type,card|nullable|date_format:m',
            'bank_name' => 'required_if:payment_type,bank|nullable|string',
            'bank_account_number' => 'required_if:payment_type,bank|nullable|numeric',
            'memo' => 'nullable|string'
        ]);

        $data['telephone'] = str_replace('-', '', $data['telephone']);
        $data['mobile'] = str_replace('-', '', $data['mobile']);
        $data['postcode'] = str_replace('-', '', $data['postcode']);
        $data['card_number'] = str_replace('-', '', $data['card_number']);
        $data['bank_account_number'] = str_replace('-', '', $data['bank_account_number']);
        if ($data['payment_type'] == 'card') {
            $data['expire_date'] = $data['expire_year'].'-'.$data['expire_month'].'-01';
        }
        unset($data['expire_year']);
        unset($data['expire_month']);

        $application->update($data);
        
        return redirect('/application/'.$application->id);
    }

    public function updateStatus(Request $request, Application $application)
    {
        $data = $request->validate([
            'status' => [
                'required',
                Rule::in(['waiting', 'accepted', 'ready_for_ship', 'sent'])
            ]
        ]);

        $application->update($data);

        return redirect('/application/'.$application->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function destroy(Application $application)
    {
        //
    }
}
