<?php

namespace App\Http\Controllers;

use App\ServiceRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ServiceRequestController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'create', 'store']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = ServiceRequest::latest();
        if (request()->has('keyword')) {
            $query->where('mobile', 'like', '%'.request('keyword'));
        }
        $requests = $query->paginate();

        return view('request/list', compact('requests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('request/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'product' => ['required', Rule::in(['LFM-100', 'ME-Y30K', 'HS-2300'])],
            'plan' => ['required', Rule::in(['10G', '20G'])],
            'contract' => ['required', Rule::in('2Y')],
            'customer_type' => ['required', Rule::in(['personal', 'business'])],
            'name' => 'required|string',
            'telephone' => 'nullable|phone',
            'mobile' => 'required|phone',
            'postcode' => 'required|numeric',
            'address' => 'required|string',
            'address_detail' => 'required|string',
            'recommander' => 'nullable|string',
            'payment_type' => ['nullable', Rule::in(['card', 'bank'])],
            'card_type' => 'required_if:payment_type,card|nullable|string',
            'card_number' => 'required_if:payment_type,card|nullable|numeric',
            'expire_year' => 'required_if:payment_type,card|nullable|date_format:Y',
            'expire_month' => 'required_if:payment_type,card|nullable|date_format:m',
            'bank_name' => 'required_if:payment_type,bank|nullable|string',
            'bank_account_number' => 'required_if:payment_type,bank|nullable|numeric',
            'memo' => 'nullable|string',
            'agree' => 'accepted'
        ]);

        $data['telephone'] = str_replace('-', '', $data['telephone']);
        $data['mobile'] = str_replace('-', '', $data['mobile']);
        $data['postcode'] = str_replace('-', '', $data['postcode']);
        $data['card_number'] = str_replace('-', '', $data['card_number']);
        $data['bank_account_number'] = str_replace('-', '', $data['bank_account_number']);
        if ($data['payment_type'] == 'card') {
            $data['expire_date'] = $data['expire_year'].'-'.$data['expire_month'].'-01';
        }
        unset($data['agree']);
        unset($data['expire_year']);
        unset($data['expire_month']);

        $serviceRequest = ServiceRequest::create($data);

        return view('request/created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ServiceRequest  $serviceRequest
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceRequest $serviceRequest)
    {
        dd($serviceRequest);
        return view('request/show', compact('serviceRequest'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ServiceRequest  $serviceRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(ServiceRequest $serviceRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ServiceRequest  $serviceRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ServiceRequest $serviceRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ServiceRequest  $serviceRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServiceRequest $serviceRequest)
    {
        //
    }
}
