<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Application;
use App\CarouselImage;
use App\Consult;
use App\Notice;

class IntroController extends Controller
{
    public function index()
    {
    	$applications = Application::latest()->limit(6)->get();
    	$images = CarouselImage::all();
    	$consults = Consult::latest()->limit(4)->get();
    	$notices = Notice::latest()->limit(4)->get();
    	if (auth()->guest()) {
            $applications->map(function ($application) {
                $length = mb_strlen($application->name);
                switch ($length) {
                    case 1:
                        $application->name = '*';
                        break;
                    case 2:
                        $application->name = mb_substr($application->name, 0, 1).'*';
                        break;
                    default:
                        $application->name = mb_substr($application->name, 0, 1).str_repeat('*', $length - 2).mb_substr($application->name, -1);
                        break;
                }
            });
        }
    	return view('intro', compact('applications', 'images', 'consults', 'notices'));
    }
}
