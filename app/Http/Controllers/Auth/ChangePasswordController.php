<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChangePasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('auth.passwords.change');
    }

    public function update()
    {
        request()->validate([
            'password' => 'required|string|min:6|confirmed'
        ]);

        $user = auth()->user();
        $user->password = bcrypt(request('password'));
        $user->save();

        return view('auth.passwords.changed');
    }
}
