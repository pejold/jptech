<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth::routes();
Route::group(['prefix' => 'admin', 'namespace' => 'Auth'], function () {
    Route::get('login', 'LoginController@showLoginForm');
    Route::post('login', 'LoginController@login')->name('login');
    Route::get('logout', 'LoginController@logout');
    Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'RegisterController@register');
    Route::get('edit', 'ChangePasswordController@index')->name('changePassword');
    Route::post('edit', 'ChangePasswordController@update');
});

Route::get('/', 'IntroController@index');
Route::resource('/product', 'ProductController');
Route::resource('/carousel', 'CarouselImageController');
Route::get('/banner/edit', 'BannerController@edit');
Route::put('/banner', 'BannerController@update');

Route::put('/application/{application}/status', 'ApplicationController@updateStatus');
Route::resource('/application', 'ApplicationController');

Route::post('/consult/{consult}', 'ConsultController@show');
Route::put('/consult/{consult}/answer', 'ConsultController@answer')->middleware('auth');
Route::resource('/consult', 'ConsultController');

Route::resource('/notice', 'NoticeController');