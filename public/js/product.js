$(function () {
    $('#description_field').summernote({
        minHeight: '300px',
        lang: 'ko-KR'
    });

    $('#banner_image_field').change(function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(this.files[0]);
        }
    });
});